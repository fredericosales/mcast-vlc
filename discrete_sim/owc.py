#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""

# import
import simpy as sim
import random as rdm

class Vlc(object):
    """
    Simulador de eventos discreto
    Frederico Sales
    <frederico.sales@engenharia.ufjf.br>
    2021
    """

    def __init__(self, pacotes, tempo_formacao, tempo_medio_processamento):
        """
        env = ambiente -> vlc
        nurse = resource -> pacote 

        parametros
        wl_inter = tempo_formacao
        mean_consult = tempo_medio_processamento
        """
        self.pacotes = pacotes
        self.tempo_formacao = tempo_formacao
        self.tempo_medio_processamento = tempo_medio_processamento
        self.vlc = simpy.Enrironment()
        self.pacote = simpy.Resource(self.vlc, capacity=self.pacotes)
        self.id_pacote = None

    def gerador_de_pacotes(self, self.vlc, self.tempo_formacao, self.pacotes):
        """
        """
        self.id_pacote = 0

        # gerador infinito de pacotes
        while True:
            instancia = gerador_de_cabecalhos(self.vlc, self.tempo_formacao, self.pacotes, self.id_pacote):

            # executa o gerador de cabecalho para esse cabecalho
            self.vlc.process(instancia)

            # amostra de tempo antes do proximo cabecalho
            tempo = random.expovariate(1.0/instancia)

            # congela ate que o tempo passe
            yield self.vlc.timeout(tempo)

            # incrementa o cabecalho
            self.id_pacote +=1

    def gerador_de_cabecalhos(self, self.vlc, self.tempo_medio_processamento, self.pacotes, self.id_pacote):
        """
        """
        entrada_na_fila_cabecalho = self.vlc.now()
        print("Cabecalho ", self.id_pacote, " entrou na fila em: ",
            entrada_na_fila_cabecalho, sep="")

        # requisicao de cabecalho
        with self.pacotes.request() as req:
            yield req

            # calcula o tempo na fila
            tempo_na_fila_cabecalho = self.vlc.now
            print("Cabecalho ", self.id_pacote, " deixou a fila em ",
                tempo_na_fila_cabecalho, sep="")

            # calcula o tempo de espera por cabecalho
            temo_de_espera_cabecalho = (tempo_na_fila_cabecalho -
                                        temo_de_espera_cabecalho)
            print("Cabecalho ", self.id_pacote, " o pacote esporou por  ",
                temo_de_espera_cabecalho, sep="")