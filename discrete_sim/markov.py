#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
# 2021
#
# if '__name__' == '__main__':
# 	transition_prob = { 
# 						'success': {'success': 0.80, 'fail': 0.19, 'dealy': 0.01},
#                         'fail':    {'success': 0.20, 'fail': 0.70, 'dealy': 0.10},
#                         'delay':   {'success': 0.10, 'fail': 0.20, 'dealy': 0.70}
#                     }
#     flux_chain = Markov(transition_prob=transition_prob)
#     flux_chain.next_state(current_state='success')
#     flux_chain.next_state(current_state='delay')
#     flux_chain.generate_states(current_state='success', n0=20)
"""

# import
import numpy as np


class Markov(object):
	"""
	"""
	def __init__(self, transition_prob):
		"""
		"""
		self.transition_prob = transition_prob
		self.states = list(transition_prob.keys())

	def next_state(self, current_state):
		"""
		"""
		return np.random.choice(
								self.states,
								p=[self.transition_prob[current_state][next_state]
								for next_state in self.states])

	def generate_states(self, current_state, no=20):
		"""
		"""
		future_states = []
		for i in range(no):
			next_state = self.next_state(current_state)
			future_states.append(next_state)
			current_state = next_state

		return future_states