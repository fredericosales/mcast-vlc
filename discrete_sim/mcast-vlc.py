#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
# 2021
"""

import random
import simpy


class MCAST-VLC(object):
	"""
	Muticast in VLC environment, the stream allow one process sent
	data to many groups.
	"""
	def __init__(self, env, capacity=simpy.core.Infinity):
		"""
		"""
		self.env = env
		self.capacity = capacity
		self.stream = []
		self.RANDOM_SEED = 60
		self.SIM_TIME = 700

	def cast(self, value):
		"""
		"""
		if not self.stream:
			raise RuntimeError("There is no mcast-vlc groups.")
		events = [store.cast(value) for store in self.cast]
		return self.env.all_of(events)

	def group_connection(self):
		"""
		Connect a group with a mcast server.
		"""
		cast = simpy.Store(self.env, capacity=self.capacity)
		self.stream.append(cast)
		return cast


	def stream_generator(name, env, out_cast):
		"""
		Generate random messages.
		"""
		while True:
			yield env.timeout(random.randint(6,10))
			msg = (env.now, '%s data stream at %d' % (name, env, now))
			out_cast.cast(msg)


	def group_consumer(self, env, in_group):
		"""
		Consume messages
		"""
		while True:
			msg = yield in_group.cast()

			if msg[0] < env.now:
				print('Receiving stream: at time %d: %s received cast: %s' % (env.now, name, msg[1]))
			else
				print('at time %d: %s received cast: %s.' % (env.now, name, msg[1]))

			yield env.timeout(random.randint(4, 8))

	def run_sim(self):
		"""
		Run the SIM.
		"""
		random.seed(self.RANDOM_SEED)
		env = simpy.Environment()
		cast = simpy.Store(env)
		env.process(self.stream_generator('stream A', env, cast))
		env.process(self.group_consumer('consumer A', env, cast))
		env.run(until=self.SIM_TIME)