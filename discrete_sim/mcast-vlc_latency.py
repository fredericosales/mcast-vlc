#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
# 2022
#
# 2 micro seconds of latency
# env = simpy.Environment()
# vlc = Owc(env, 2)
# env.process(vlc.server(env, vlc))
# env.process(vlc.consumer(env, vlc))
# env.run(until=700)
#
"""

# import 
import simpy
import random

class Owc(object):
	"""
	"""

	def __init__(self, env, delay):
		"""
		"""
		self.env = env
		self.delay = delay
		self.store = simpy.Store(env)
		self.SIM_TIME = 700

	def latency(self, value):
		"""
		"""
		yield self.env.timeout(self.delay)
		self.store.put(value)

	def cast(self, value):
		"""
		"""
		self.env.process(self.latency(value))

	def consume(self):
		"""
		"""
		return self.store.get()

	def server(self, vlc):
		"""
		"""
		while True:
			yield self.env.timeout(2)
			vlc.self.cast()

	def consumer(self, vlc):
		"""
		"""
		while True:
			msg = yield vlc.self.consume()
			print("receveid package at %d while %s" % (self.env.now, msg))