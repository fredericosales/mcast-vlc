/*
 Copyright (c) 2014, IMDEA NETWORKS Institute
 
 This file is part of the OpenVLC's source codes.
 
 OpenVLC's source codes are free: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenVLC's source codes are distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with the source codes of OpenVLC.  If not, see <http://www.gnu.org/licenses/>.
 */
////////////////////////
#include <linux/sched.h>
#include <linux/interrupt.h> /* mark_bh */
#include <linux/in.h>
#include <linux/netdevice.h>   /* struct device, and other headers */
#include <linux/inetdevice.h>
#include <linux/skbuff.h>
#include <linux/in6.h>
#include <linux/delay.h>
#include <linux/miscdevice.h>
#include <linux/gpio.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/fs.h>
#include <asm/segment.h>
#include <linux/buffer_head.h>
#include <linux/types.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/ioctl.h>
#include <linux/cdev.h>
#include <linux/string.h>
#include <linux/list.h>
#include <linux/pci.h>
#include <asm/uaccess.h>
#include <asm/atomic.h>
#include <asm/unistd.h>
#include <asm/io.h>
#include <asm/system.h>
#include <linux/iio/consumer.h>
#include <linux/iio/iio.h>
#include <asm/checksum.h>
#include <linux/kthread.h>
#include <linux/err.h>
#include <linux/wait.h>
#include <linux/timex.h>
#include <linux/clk.h>
#include <linux/pinctrl/consumer.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
#include <linux/jiffies.h>
#include <linux/module.h>
#include <rtdm/rtdm_driver.h>
#include <asm/io.h>  // ioremap

#include <linux/proc_fs.h>

#include <linux/skbuff.h>
#include <linux/ip.h>          /* struct iphdr */
#include <linux/tcp.h>         /* struct tcphdr */
#include <linux/if_arp.h>
#include <linux/rslib.h>

#include "openvlc.h"
#define DEVICE_NAME "mcast-srv"

MODULE_AUTHOR("Qing WANG");
MODULE_LICENSE("Dual BSD/GPL");

volatile void* gpio1;
volatile void* gpio2;

#define MAC 0
#define APP 1
static int mac_or_app = 0;
module_param(mac_or_app, int, 0);

static int block_size = 200;
module_param(block_size, int, 0);

unsigned long jStart, jEnd, jStartU_global, jEndU_global;
uint16_t par[ECC_LEN];
int ecc_symsize = 8;
int ecc_poly = 0x11d;

/* the Reed Solomon control structure */
static struct rs_control *rs_decoder;

// Timer: used to trigger sending/receiving symbols
static rtdm_timer_t phy_timer;
static _Bool f_adjust_slot = 0;

#define MAX_RETRANSMISSION  2 // At most tx 4 times

static int cnt_retransmission = 0;
static _Bool f_re_tx = 0;

static int frq = 50; // Unit: K
static int tx = 0;
static int rx = 0;

static int tx_len = 20;
static int rx_len = 1;
static int mode = 0;
static int threshold = 5;
static int show_msg = 0;
static int flag_exit = 0;
static int flag_lock = 0;
module_param(flag_lock, int, 0);

static int mtu = 1200;


static unsigned int self_id = 0;
static unsigned int self_ip = 0;
static unsigned int dst_id = 0;

#define MAX_DETECTION_BIT 10

module_param(self_id, int, 0);
module_param(dst_id, int, 0);
module_param(tx, int, 0);
module_param(rx, int, 0);
module_param(frq, int, 0);
module_param(rx_len, int, 0);
module_param(tx_len, int, 0);
module_param(mode, int, 0);
module_param(threshold, int, 0);
module_param(show_msg, int, 0);
module_param(mtu, int, 0);


// Qing - May 2, 2015
//static int pd_as_rx = 0;
//module_param(pd_as_rx, int, 0);
static int hpl = 0;
module_param(hpl, int, 0);
static int bit_led_anode = BIT_LED_ANODE;

// May 28, 2015
struct proc_dir_entry *vlc_dir, *tx_device, *rx_device;
static int rx_device_value = 0;
static int tx_device_value = 0;

const char *name = "any";

static struct task_struct *task_mac_tx = NULL;
static struct task_struct *task_phy_decoding = NULL;

#define ACK_LEN 1

static _Bool f_ready_to_tx = 0;
//static _Bool f_rx_one_preamble = 0;

#define RX_BUFFER_SIZE 10000
static int rx_buffer[RX_BUFFER_SIZE] = {0};
static int rx_in_index = 0;
static int rx_out_index = 0;
//static char *rx_data = NULL;
#define RX_DATA_LEN 2000
static char rx_data[RX_DATA_LEN]; // Length > MAC_HDR_LEN+maxpayload+rs_code;

//static int *rx_data_symbol = NULL;
static int tmp_symbol_index = 0;
static int rx_in_val = 0;

// For DATA frame
//static unsigned char* data_buffer_byte = NULL;
//static _Bool* data_buffer_symbol = NULL;
#define DB_BYTE_LEN 2000
#define DB_SYMBOL_LEN 2000*16
static unsigned char data_buffer_byte[DB_BYTE_LEN];
static _Bool data_buffer_symbol[DB_SYMBOL_LEN];

static int data_buffer_byte_len = 0;
static int data_buffer_symbol_len = 0;
static int tx_data_curr_index = 0;
// For ACK frame
//static char* ack_buffer_byte = NULL;
//static _Bool* ack_buffer_symbol = NULL;

#define ACK_BYTE_LEN 50
#define ACK_SYMBOL_LEN 50*16
static char ack_buffer_byte[ACK_BYTE_LEN];
static _Bool ack_buffer_symbol[ACK_SYMBOL_LEN];

static int ack_buffer_byte_len = 0;
static int ack_buffer_symbol_len = 0;
static int tx_ack_curr_index = 0;



// ReqId variables
#define REQID_BYTE_LEN 50
#define REQID_SYMBOL_LEN 50*16
static char reqid_buffer_byte[REQID_BYTE_LEN];
static _Bool reqId_buffer_symbol[REQID_SYMBOL_LEN];

static int reqId_buffer_byte_len = 0;
static int reqId_buffer_symbol_len = 0;
static int tx_reqId_curr_index = 0;


#define REPID_BYTE_LEN 50
#define REPID_SYMBOL_LEN 50*16
static char repId_buffer_byte[REPID_BYTE_LEN];
static _Bool repId_buffer_symbol[REPID_SYMBOL_LEN];

static int repId_buffer_byte_len = 0;
static int repId_buffer_symbol_len = 0;
static int tx_repId_curr_index = 0;


#define RREQ_BYTE_LEN 200
#define RREQ_SYMBOL_LEN 200*16
static char rreq_buffer_byte[RREQ_BYTE_LEN];
static _Bool rreq_buffer_symbol[RREQ_SYMBOL_LEN];

static int rreq_buffer_byte_len = 0;
static int rreq_buffer_symbol_len = 0;
static int tx_rreq_curr_index = 0;


#define RERR_BYTE_LEN 100
#define RERR_SYMBOL_LEN 100*16
static char rerr_buffer_byte[RERR_BYTE_LEN];
static _Bool rerr_buffer_symbol[RERR_SYMBOL_LEN];

static int rerr_buffer_byte_len = 0;
static int rerr_buffer_symbol_len = 0;
static int tx_rerr_curr_index = 0;



#define RREP_BYTE_LEN 200
#define RREP_SYMBOL_LEN 200*16
static char rrep_buffer_byte[RREP_BYTE_LEN];
static _Bool rrep_buffer_symbol[RREP_SYMBOL_LEN];

static int rrep_buffer_byte_len = 0;
static int rrep_buffer_symbol_len = 0;
static int tx_rrep_curr_index = 0;

int max_fails = 0;
int backoff_extra = 1;


int staticRoute = 0;


int free_position = 0;
int broadcast_id = 0xff;

#define MAX_SYMBOLS_SENSING 200

int free_position_sensing = 0;
int buffer_sensing[MAX_SYMBOLS_SENSING];

// Feb. 25, 2014
static int decoding_sleep_slot = 1;

uint32_t slot_ns;

// May 27, 2015
static int adc_ch = 0;
module_param(adc_ch, int, 0);
#define RX_FROM_LED 0x00 // channel 0
#define RX_FROM_PD 0x01 // channel 1
//static int prev_hpl = 0;

static int bit_clc = 1 << BIT_CLC;

// April 04

enum {
    SENSING, RX, TX
} phy_state; // Short sensing is implemented in tx state

#define LONG_SENSING_SYMBOLS 8
static int sensing_buffer[LONG_SENSING_SYMBOLS] = {0};
static int index_long_sensing = 0;
static int avg_val_from_sensing = 0;
static int min_threshold = 10; // Pre-defined minimal threshold between ON and OFF

static int CW = 0; // Contention window
static int CW_min = 4; // Minimal contention window
static int CW_max = 16; // Maximal contention window

static int ACK_TIMEOUT = 2 * 100 * 8 * 2; // Ack length is 10 bytes: times*bytes*bits_per_byte*manchester


static _Bool f_ready_to_tx_ack = 0;
static _Bool f_ready_to_tx_repId = 0;
static _Bool f_ready_to_tx_reqId = 0;
static _Bool f_ready_to_tx_rreq = 0;
static _Bool f_ready_to_tx_rrep = 0;
static _Bool f_ready_to_tx_rerr = 0;

static _Bool f_wait_for_ack = 0;

static int index_ack_timeout = 0;
static int index_rreq_timeout = 0;
static int index_reqId_timeout = 0;
static int index_repId_timeout = 0;
static _Bool f_dropped_frame = 0;
static _Bool f_received_ack = 0;

static int forwardRreq = 0;

static _Bool f_received_reqId = 0;
static _Bool f_received_repId = 0;
static _Bool f_received_rreq = 0;
static _Bool f_received_rrep = 0;
static _Bool f_received_rerr = 0;


static _Bool f_wait_for_rrep = 0;
static _Bool f_rreq_timeout = 0;
static int f_rreq_timeout_count = 0;

static _Bool build_routing_table = 1;
static _Bool setup_phase = 1;



//static int ip_dest_static = 134260928;


// The device
struct net_device *vlc_devs;

// This structure is private to each device. It is used to pass
// packets in and out, so there is place for a packet

struct vlc_priv {
    struct net_device_stats stats;
    int status;
    struct vlc_packet *ppool;
    int rx_int_enabled;
    struct vlc_packet *tx_queue; // List of tx packets
    int tx_packetlen;
    u8 *tx_packetdata;
    struct sk_buff *skb;
    spinlock_t lock;
    struct net_device *dev;
};

void __aeabi_d2uiz(void) {
}

void __aeabi_ddiv(void) {
}

void __aeabi_i2d(void) {
}

int vlc_tx(struct sk_buff *skb, struct net_device *dev);

static struct vlchdr *vlc_hdr(const struct sk_buff *skb) {
    return (struct vlchdr *) skb_mac_header(skb);
}

// Table for entries
struct arp_table table[TABLE_SIZE];
struct routing_table routing_table[TABLE_SIZE];
/*
 * A structure representing an in-flight packet.
 */
struct vlc_packet {
    struct vlc_packet *next;
    struct net_device *dev;
    int destination;
    int source;
    int datalen;
    u8 data[2000 + 10];
};

struct file *file;

struct vlc_packet *tx_pkt;
struct vlc_packet *rx_pkt_check_dup;
struct vlc_packet *rx_pkt;
struct vlc_packet *tmp_pkt;

int pool_size = 10;
module_param(pool_size, int, 0);



static void print_ip(uint32_t addr) {

    unsigned char bytes[4];
    bytes[0] = addr & 0xFF;
    bytes[1] = (addr >> 8) & 0xFF;
    bytes[2] = (addr >> 16) & 0xFF;
    bytes[3] = (addr >> 24) & 0xFF;
    printk(" %d.%d.%d.%d", bytes[0], bytes[1], bytes[2], bytes[3]);

}

static void expungeRoute(int destination){
    int i =0;
    for(i = 0; i< TABLE_SIZE; i++){
        if(routing_table[i].dest_address == destination){
            f_rreq_timeout_count = 0;
            routing_table[i] = (const struct routing_table){ 0 };
        }
    }    
}


int get_next_hop_from_routingTable(int destination){
    int i =0;
    int hop = 0;
    for(i = 0; i< TABLE_SIZE; i++){
        if(routing_table[i].dest_address == destination && routing_table[i].next_hop != 0){
            hop = routing_table[i].next_hop;
        }        
    }    
    return hop;
}

int get_hop_count_from_routingTable(int destination){
    int i =0;
    int hop = 0;
    for(i = 0; i< TABLE_SIZE; i++){
        if(routing_table[i].dest_address == destination && routing_table[i].next_hop != 0){
            hop = routing_table[i].hop_count;
        }        
    }    
    return hop;
}



static void printTable(void){
    int i;
//    printk("Index\tNextHop\t\tSeq_n\tHop\tMAX HOP\tDest IP\t\tSTATUS\n");
    for(i=0;i<TABLE_SIZE; i++){
        if(routing_table[i].dest_address == 0){
            continue;
        }
//        printk("%d\t\t", i);    
//        print_ip(routing_table[i].next_hop); printk("\t");
//        printk("%d\t", routing_table[i].sequence_number);   
//        printk("%d\t", routing_table[i].hop_count);    
//        printk("%d\t", routing_table[i].msg_hop_limit);    
//        print_ip(routing_table[i].dest_address); printk("\t");
//        printk("%d\t", routing_table[i].state); printk("\n");
    }
    
}


int update_seqNumber_from_routingTable(int destination){
    int i =0;
    int seq = 0;
    for(i = 0; i< TABLE_SIZE; i++){
        if(routing_table[i].dest_address == destination){
            routing_table[i].sequence_number++;
            seq = routing_table[i].sequence_number;
//            printk("Sequence number to node ");print_ip(destination);printk(" is %d", seq);printk("\n");
            return seq;
        }
    }
    return seq;
}


int getIdFromArpTable(int ip){
    int i;
    for (i = 0; i < TABLE_SIZE; i++) {
        if (table[i].ip_address == ip) {
            return table[i].mac_addr;
        }
    }    
    return 0;
}


//Decrease expiration time from table entries. If it reaches 0, route is broken.
static void countdownRoutingTable(void){
    int i =0;
    for(i = 0; i< TABLE_SIZE; i++){
        if(routing_table[i].dest_address!=0){
            if(staticRoute){
                if(routing_table[i].lifeTime-- <= 0){
                    printk("Life time over - Expunged: "); print_ip(routing_table[i].dest_address); printk(" ---\n");
                    expungeRoute(routing_table[i].dest_address);
                    return;
                }
            }
            if(routing_table[i].state == ACTIVE){
                if(routing_table[i].expirationTime-- <= 0){
                    //Route expired
                    printk("ACTIVE TO IDLE: "); print_ip(routing_table[i].dest_address); printk(" ---\n");
                    routing_table[i].state = IDLE;
                    routing_table[i].expirationTime = MAX_IDLETIME;
                }
            } 
            else if (get_next_hop_from_routingTablerouting_table[i].state == IDLE) {
                if(routing_table[i].expirationTime-- <= 0){
                    //Route expired
                    printk("IDLE TO EXPIRED: "); print_ip(routing_table[i].dest_address); printk(" ---\n");
                    routing_table[i].state = EXPIRED;
                    routing_table[i].expirationTime = MAX_SEQNUM_LIFETIME;
                }                
            } 
            else if (routing_table[i].state == EXPIRED) {
                if(routing_table[i].expirationTime-- <= 0){
                    //Route expired
                    printk("EXPIRED TO EXPUNGED: "); print_ip(routing_table[i].dest_address); printk(" ---\n");
                    expungeRoute(routing_table[i].dest_address);
                }                
            }
            
        }
    }
}

static void breakRouteToNode(int ip_dest){
    int i =0;
    for(i = 0; i< TABLE_SIZE; i++){
        if(routing_table[i].dest_address == ip_dest){
            routing_table[i].state = BROKEN;
//            printk(" --- BREAKING ROUTE TO DESTINATION "); print_ip(ip_dest); printk(" ---\n");
        }        
    }
//    printTable();
}

static _Bool routeIsFresh(int dst_addr, int seq_n){
    int i;
    for(i = 0; i< TABLE_SIZE; i++){
        if(routing_table[i].dest_address == dst_addr && routing_table[i].sequence_number <= seq_n){
            return false;
        } else {
            return true;
        } 
    }    
    return false;
}

static int getNextFreePosition(){ 
    int i = 0;
    for(i = 0; i< TABLE_SIZE; i++){
        if(routing_table[i].dest_address == 0){
            printk("Next free position: %d\n", i);
            return i;
        }
    }
    return TABLE_SIZE+1;
}

static _Bool checkRerr(int destination, int source){
    int i =0;
    for(i = 0; i< TABLE_SIZE; i++){
        if(routing_table[i].dest_address == destination){
            if(routing_table[i].next_hop == source){
                return 1;
            } else {
                return 0;
            }
        }     
    }
    return 0;     
}


static _Bool checkIfNodeInRoutingTable(int destination){    
    int i =0;
    for(i = 0; i< TABLE_SIZE; i++){
//        if(routing_table[i].dest_address == 0)
//            printk("%d", routing_table[i].dest_address);
        if(routing_table[i].dest_address == destination){
            if(staticRoute){
                return true;
            }
            
            if(routing_table[i].state == ACTIVE){
                routing_table[i].expirationTime = ACTIVE_INTERVAL;                
            } else if(routing_table[i].state == IDLE){
                printk("IDLE TO ACTIVE: "); print_ip(routing_table[i].dest_address); printk(" ---\n");
                routing_table[i].state = ACTIVE;
                routing_table[i].expirationTime = ACTIVE_INTERVAL;                
            } else if(routing_table[i].state == BROKEN || routing_table[i].state == EXPIRED){
                printk("Node is in routing table, but route is broken or expired..\n");
                return false;    
            }
            return true;
        }     
    }
    return false;    
}


// here I have to send group and group leader
static void addNodeToRoutingTable(int dest_ip, int next_hop, int sequence_number, int hopC, int max_hop){
    int co, contains, index;
    index = getNextFreePosition();
    contains = 0;
    for (co = 0; co < TABLE_SIZE; co++) {
        if (routing_table[co].dest_address == dest_ip) {
            printk("Table already contains node ");
            print_ip(dest_ip);printk("\n");
            contains = 1;
            if(routing_table[co].state == IDLE || routing_table[co].state == ACTIVE){
                printk(" --- UPDATING ROUTING TABLE ---\n");
                index = co;
            } else {
                //node is broken
                index = co;
            }
        }
    }
    
    routing_table[index].dest_address = dest_ip;
    routing_table[index].next_hop = next_hop;
    routing_table[index].sequence_number = sequence_number;  
    routing_table[index].hop_count = hopC;   
    routing_table[index].msg_hop_limit = max_hop;  
    routing_table[index].expirationTime = ACTIVE_INTERVAL;  
    routing_table[index].lifeTime = LIFETIME_TIMEOUT;     
    routing_table[index].state = ACTIVE;
    
    printk("Added node "); print_ip(dest_ip); printk(" to routing table\n");
       
    
    if(!contains){
        printk(" --- ADDING NEW ENTRY TO ROUTING TABLE ---\n");
    }
    
    printTable();

}

static void addNodeToArpTable(int src_id, int src_ip, int average){
    int i,co, contains; 

    contains = 0;
    for (co = 0; co < TABLE_SIZE; co++) {
        if (table[co].mac_addr == src_id) {
            printk("Table already contains node ");
            print_ip(src_ip);printk("\n");
            table[co].signal_strength = average;
            contains = 1;
            break;
        }
    }
    
    if (!contains) {
        table[free_position].mac_addr = src_id;
        table[free_position].ip_address = src_ip;
        table[free_position].signal_strength = average;
        printk("Added to arp table and send REP_ID back.\n");
        free_position++;
    }
    
    printk("Index\tID\tSignal\tIP Address\n");
    for(i=0;i<TABLE_SIZE; i++){
        printk("%d\t\t%d\t%d\t", i, table[i].mac_addr, table[i].signal_strength);      
        print_ip(table[i].ip_address);printk("\n");
    }

}


//static void vlc_tx_timeout(struct net_device *dev);
static void (*vlc_interrupt)(int, void *, struct pt_regs *);

__be16 vlc_type_trans(struct sk_buff *skb, struct net_device *dev) {
    unsigned int dst_addr_mac, src_addr_mac;
    struct vlchdr *vlc;
    skb->dev = dev;
    skb_reset_mac_header(skb);
    skb_pull_inline(skb, VLC_HLEN);
    vlc = vlc_hdr(skb);

    dst_addr_mac = vlc->h_dest[1] | vlc->h_dest[0] << 8;
    src_addr_mac = vlc->h_source[1] | vlc->h_source[0] << 8;

    if (ntohs(vlc->h_proto) >= 1536)
        return vlc->h_proto;

    return htons(VLC_P_DEFAULT);
}

/*
 * Set up a device's packet pool.
 */
void vlc_setup_pool(struct net_device *dev) {
    struct vlc_priv *priv = netdev_priv(dev);
    int i;
    struct vlc_packet *pkt;

    priv->ppool = NULL;
    for (i = 0; i < pool_size; i++) {
        pkt = kmalloc(sizeof (struct vlc_packet), GFP_KERNEL);
        if (pkt == NULL) {
            printk(KERN_NOTICE "Ran out of memory allocating packet pool\n");
            return;
        }
        pkt->dev = dev;
        pkt->next = priv->ppool;
        priv->ppool = pkt;
    }
}

//static void get_device_ip(struct net_device *dev) {
//
//    uint32_t addr;
//    addr = inet_select_addr(dev, 0, RT_SCOPE_UNIVERSE);
//
//    unsigned char bytes[4];
//    bytes[0] = addr & 0xFF;
//    bytes[1] = (addr >> 8) & 0xFF;
//    bytes[2] = (addr >> 16) & 0xFF;
//    bytes[3] = (addr >> 24) & 0xFF;
//    printk("%d.%d.%d.%d\n", bytes[0], bytes[1], bytes[2], bytes[3]);
//
//}


void vlc_teardown_pool(struct net_device *dev) {
    struct vlc_priv *priv = netdev_priv(dev);
    struct vlc_packet *pkt;
    //unsigned long flags;

    //spin_lock_bh(&priv->lock);
    while ((pkt = priv->ppool)) {
        priv->ppool = pkt->next;
        if (pkt) kfree(pkt);
        /* FIXME - in-flight packets ? */
    }
    //spin_unlock_bh(&priv->lock);
}

/*
 * Buffer/pool management.
 */
struct vlc_packet *vlc_get_tx_buffer(struct net_device *dev) {
    struct vlc_priv *priv = netdev_priv(dev);
    //unsigned long flags;
//    printk("Debug 1\n");
    struct vlc_packet *pkt;
    if (flag_lock)
        spin_lock_bh(&priv->lock);    
    
    pkt = priv->ppool;
    if(pkt == NULL){
        printk("Packet is null\n");
    }
        
//    printk("Debug 2\n");
    
    // o pau tá aqui
    priv->ppool = pkt->next;
    
//    printk("Debug 3\n");
    
    if (priv->ppool == NULL) {
//        printk (KERN_INFO "The MAC layer queue is full!\n");
        netif_stop_queue(dev);  
    }
    if (flag_lock)
        spin_unlock_bh(&priv->lock);

    return pkt;
}

void vlc_release_buffer(struct vlc_packet *pkt) {
    //unsigned long flags;
    struct vlc_priv *priv = netdev_priv(pkt->dev);

    if(pkt == NULL){
        printk("PAkcet is null\n");
    }
    if (flag_lock)
        spin_lock_bh(&priv->lock);
    pkt->next = priv->ppool;
    priv->ppool = pkt;
    if (flag_lock)
        spin_unlock_bh(&priv->lock);
    if (netif_queue_stopped(pkt->dev) && pkt->next == NULL && flag_exit == 0)
        netif_wake_queue(pkt->dev);

//    printk("Release buffer\n");
}

// Add a packet from upper layer to the beginning of the MAC queue

void vlc_enqueue_pkt(struct net_device *dev, struct vlc_packet *pkt) {
    //unsigned long flags;
    struct vlc_priv *priv = netdev_priv(dev);
    struct vlc_packet *last_pkt;

    //spin_lock_bh(&priv->lock, flags);
    //pkt->next = priv->tx_queue;  /* FIXME - misorders packets */
    //priv->tx_queue = pkt;
    //spin_unlock_bh(&priv->lock, flags);

    /// Fix the misorder packets
    if (flag_lock)
        spin_lock_bh(&priv->lock);
    last_pkt = priv->tx_queue;
    if (last_pkt == NULL) {
        priv->tx_queue = pkt; // Enqueue the new packet
    } else {
        while (last_pkt != NULL && last_pkt->next != NULL) {
            last_pkt = last_pkt->next;
        }
        last_pkt->next = pkt; // Put new the pkt to the end of the queue
    }
    if (flag_lock)
        spin_unlock_bh(&priv->lock);
    ///
}

// Departure a packet from the end of the MAC queue (FIFO)

struct vlc_packet *vlc_dequeue_pkt(struct net_device *dev) {
    //unsigned long flags;
    struct vlc_priv *priv = netdev_priv(dev);
    struct vlc_packet *pkt;

    if (flag_lock)
        spin_lock_bh(&priv->lock);
    pkt = priv->tx_queue;
    if (pkt != NULL)
        priv->tx_queue = pkt->next;
    if (flag_lock)
        spin_unlock_bh(&priv->lock);

    return pkt;
}

/*
 * Enable and disable receive interrupts.
 */
static void vlc_rx_ints(struct net_device *dev, int enable) {
    struct vlc_priv *priv = netdev_priv(dev);
    priv->rx_int_enabled = enable;
}

// Switch the LED to TX mode

static void switch_led_to_tx(void) {
    gpio_set_value(GPIO_LED_CATHODE, GPIOF_INIT_LOW);
    gpio_set_value(GPIO_BUFFER_CONTROL, GPIOF_INIT_LOW);
}

// Switch the LED to RX mode

static void switch_led_to_rx(void) {
    gpio_set_value(GPIO_LED_ANODE, GPIOF_INIT_LOW);
    gpio_set_value(GPIO_BUFFER_CONTROL, GPIOF_INIT_HIGH);
}

// Switch the TX

static void switch_tx(void) {
    //if (prev_hpl != hpl) {
    //writel(1<<bit_led_anode, gpio2+CLEAR_OFFSET);
    //prev_hpl = hpl;
    //}
    if (hpl == 1) {
        bit_led_anode = BIT_H_POWER_LED; // High-power LED as TX
        gpio_direction_output(GPIO_LED_OR_PD, GPIOF_INIT_LOW); // PD as RX
        gpio_direction_output(GPIO_LED_ANODE, GPIOF_INIT_LOW); // PD as RX
        //writel(1<<BIT_LED_ANODE, gpio2+CLEAR_OFFSET); // Clear the low-power LED
    } else { // LED
        bit_led_anode = BIT_LED_ANODE; // LED as TX
        gpio_direction_output(GPIO_LED_OR_PD, GPIOF_INIT_HIGH); // LED as RX  
        gpio_direction_output(GPIO_H_POWER_LED, GPIOF_INIT_LOW); // PD as RX
        //writel(1<<BIT_H_POWER_LED, gpio2+CLEAR_OFFSET); // Clear the HIGH-power LED
    }
}

#define SPI_DELAY_CNT 10
// Delay in terms of count

static void inline delay_n_NOP(void) {
    int i;
    for (i = SPI_DELAY_CNT; i > 0; i--)
        ;
}

// Write SFD and channel ID to the ADC

static void SPI_write_sfd_and_ch(void) {
    //unsigned char write_byte = 0x18; // 0001 1000  channel 0 of the ADC
    unsigned char write_byte = 0x18 + adc_ch; // 0001 1000  channel 0 of the AD
    unsigned char shift = 0x10; // 0001 1000
    while (shift > 0) {
        writel(bit_clc, gpio2 + CLEAR_OFFSET);
        delay_n_NOP();
        if ((_Bool) (write_byte & shift)) {
            writel(1 << BIT_MOSI, gpio2 + SET_OFFSET);
            delay_n_NOP();
        } else {
            writel(1 << BIT_MOSI, gpio2 + CLEAR_OFFSET);
            delay_n_NOP();
        }
        shift >>= 1;
        writel(bit_clc, gpio2 + SET_OFFSET);
    }
}

// Read one symbol from the ADC

static int SPI_read_from_adc(void) {
    unsigned int value = 0, index;

    writel(1 << BIT_CS, gpio1 + CLEAR_OFFSET);
    delay_n_NOP();
    SPI_write_sfd_and_ch();
    // Skip the first interval
    writel(bit_clc, gpio2 + CLEAR_OFFSET);
    delay_n_NOP();
    writel(bit_clc, gpio2 + SET_OFFSET);
    delay_n_NOP();
    // Read the value
    for (index = 0; index < 11; index++) {
        writel(bit_clc, gpio2 + CLEAR_OFFSET);
        delay_n_NOP();
        value <<= 1;
        value |= (0x1 & (readl(gpio1 + READ_OFFSET) >> BIT_MISO));
        writel(bit_clc, gpio2 + SET_OFFSET);
        delay_n_NOP();
    }
    writel(bit_clc, gpio2 + CLEAR_OFFSET);
    delay_n_NOP();
    writel(1 << BIT_CS, gpio1 + SET_OFFSET);
    delay_n_NOP();

    return value;
}

//#define POLY 0x8408

/*
 *                                      16   12   5
 * this is the CCITT CRC 16 polynomial X  + X  + X  + 1.
 * This works out to be 0x1021, but the way the algorithm works
 * lets us use 0x8408 (the reverse of the bit pattern).  The high
 * bit is always assumed to be set, thus we only use 16 bits to
 * represent the 17 bit value.
 * 1 0001 0000 0010 0001
 * 1000010000000001
 */
unsigned short crc16(char *data_p, unsigned short length) {
    unsigned char i;
    unsigned int data;
    unsigned int crc = 0xffff;
    unsigned int poly = 0x8408;

    if (length == 0)
        return (~crc);
    do {
        for (i = 0, data = (unsigned int) 0xff & *data_p++; i < 8; i++, data >>= 1) {
            if ((crc & 0x0001) ^ (data & 0x0001))
                crc = (crc >> 1) ^ poly;
            else crc >>= 1;
        }
    } while (--length);

    crc = ~crc;
    data = crc;
    crc = (crc << 8) | (data >> 8 & 0xff);

    return (crc);
}


//// Print a (new generated) frame
// static void print_uncoded_frame(void)
// {
//     int i = 0;
//     printk("Generated:");
//     for (i=PREAMBLE_LEN; i<data_buffer_byte_len; i++)
//         printk(" %02x",data_buffer_byte[i]&0xff);
//     printk("\n");
// }

//// Print a (new generated) frame
//static void print_uncoded_ack(void)
//{
//int i = 0;
//printk("Generated:");
//for (i=PREAMBLE_LEN; i<ack_buffer_byte_len; i++)
//printk(" %02x",ack_buffer_byte[i]&0xff);
//printk("\n");
//}


//// Print a (new generated) frame
// static void print_coded_frame(void)
// {
//     int i = 0;
//     printk("coded frame:");
//     for (i=0; i<data_buffer_symbol_len; i++)
//         printk(" %1x",data_buffer_symbol[i]&0xff);
//     printk("\n");
// }

// On-Off Keying (OOK) WITH Manchester Run-Length-Limited (RLL) code

static void OOK_with_Manchester_RLL(char *buffer_before_coding,
        _Bool *buffer_after_coding, int len_before_coding) {
    int byte_index, symbol_index = 0;
    unsigned char curr_byte, mask;

    // Convert the preamble -- OOK w/o Manchester RLL code
    for (byte_index = 0; byte_index < PREAMBLE_LEN; byte_index++) {
        mask = 0x80;
        curr_byte = buffer_before_coding[byte_index] & 0xff;
        while (mask) {
            buffer_after_coding[symbol_index++] = (_Bool) (curr_byte & mask);
            mask >>= 1;
        }
    }
    // Convert the parts after the preamble -- OOK w Manchester RLL code
    for (byte_index = PREAMBLE_LEN; byte_index < len_before_coding; byte_index++) {
        mask = 0x80;
        curr_byte = buffer_before_coding[byte_index] & 0xff;
        while (mask) {
            if ((_Bool) (curr_byte & mask)) { // Bit 1 -- LOW-HIGH
                buffer_after_coding[symbol_index++] = false;
                buffer_after_coding[symbol_index++] = true;
            } else { // Bit 0 -- HIGH-LOW
                buffer_after_coding[symbol_index++] = true;
                buffer_after_coding[symbol_index++] = false;
            }
            mask >>= 1;
        }
    }
}


// Simple method to send a request address to neighbours

static void construct_reqId(char* buffer) {
    int i;
    uint32_t addr;
    
    for (i = 0; i < PREAMBLE_LEN; i++)
        buffer[i] = 0xaa; // Preamble
    // SFD
    buffer[PREAMBLE_LEN] = 0x6a;

    addr = inet_select_addr(vlc_devs, 0, RT_SCOPE_UNIVERSE);
    self_ip = addr;

//    printk("Destination id: %d | Source id: %d \n", broadcast_id, self_id);
//    print_ip(self_ip);
                    
    // Destination id
    buffer[PREAMBLE_LEN + 1] = (unsigned char) ((broadcast_id >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 2] = (unsigned char) (broadcast_id & 0xff);
    
    // Source id
    buffer[PREAMBLE_LEN + 3] = (unsigned char) ((self_id >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 4] = (unsigned char) (self_id & 0xff);
        
    // Source Address
    buffer[PREAMBLE_LEN + 5] = (unsigned char) ((self_ip >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 6] = (unsigned char) ((self_ip >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 7] = (unsigned char) ((self_ip >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 8] = (unsigned char) (self_ip & 0xff);

}


// Simple method to send a reply to request address to neighbors

static void construct_repId(char* buffer, int source) {
    int i;
    
    for (i = 0; i < PREAMBLE_LEN; i++)
        buffer[i] = 0xaa; // Preamble
    // SFD
    buffer[PREAMBLE_LEN] = 0x7a;
    
    // Destination id
    buffer[PREAMBLE_LEN + 1] = (unsigned char) ((source >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 2] = (unsigned char) (source & 0xff);
    
    // Source id
    buffer[PREAMBLE_LEN + 3] = (unsigned char) ((self_id >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 4] = (unsigned char) (self_id & 0xff);
    
    // Source Address
    buffer[PREAMBLE_LEN + 5] = (unsigned char) ((self_ip >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 6] = (unsigned char) ((self_ip >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 7] = (unsigned char) ((self_ip >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 8] = (unsigned char) (self_ip & 0xff);

}

//Method to build first RREQ frame
static void construct_rreq(char* buffer, int ip_dest) {
    
    int hopCount = 1;
    unsigned int ip_len = 0;
    uint32_t addr;
    int seq_number;

    // 3232235525 - 192.168.0.5
    // 3232235524 - 192.168.0.4
    // 3232235529 - 192.168.0.9
    // 3232235522 - 192.168.0.2

    addr = inet_select_addr(vlc_devs, 0, RT_SCOPE_UNIVERSE);
    self_ip = addr;        
    
    //msg-hop-limit    
    buffer[PREAMBLE_LEN + 1] = (unsigned char) ((MAX_HOP >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 2] = (unsigned char) ((MAX_HOP >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 3] = (unsigned char) ((MAX_HOP >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 4] = (unsigned char) (MAX_HOP & 0xff);    
    
    // Source Address
    buffer[PREAMBLE_LEN + 5] = (unsigned char) ((hopCount >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 6] = (unsigned char) ((hopCount >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 7] = (unsigned char) ((hopCount >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 8] = (unsigned char) (hopCount & 0xff);

    // Destination Address
    buffer[PREAMBLE_LEN + 9] = (unsigned char) ((self_ip >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 10] = (unsigned char) ((self_ip >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 11] = (unsigned char) ((self_ip >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 12] = (unsigned char) (self_ip & 0xff);
        
    //Length of addr
    buffer[PREAMBLE_LEN + 13] = (unsigned char) ((ip_dest >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 14] = (unsigned char) ((ip_dest >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 15] = (unsigned char) ((ip_dest >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 16] = (unsigned char) (ip_dest & 0xff);
    
    //Get actual seq_number for route. Increase 1.
    seq_number = update_seqNumber_from_routingTable(ip_dest);
    
    //Length of addr
    buffer[PREAMBLE_LEN + 17] = (unsigned char) ((seq_number >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 18] = (unsigned char) ((seq_number >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 19] = (unsigned char) ((seq_number >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 20] = (unsigned char) (seq_number & 0xff);
    
    //Length of addr
    buffer[PREAMBLE_LEN + 21] = (unsigned char) ((ip_len >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 22] = (unsigned char) ((ip_len >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 23] = (unsigned char) ((ip_len >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 24] = (unsigned char) (ip_len & 0xff);
    
}


//Method to build first RREQ frame
static void construct_rerr(char* buffer, int ip_unr) {
    
    int hopCount = 1;

    // Source Address
    buffer[PREAMBLE_LEN + 1] = (unsigned char) ((MAX_HOP >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 2] = (unsigned char) ((MAX_HOP >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 3] = (unsigned char) ((MAX_HOP >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 4] = (unsigned char) (MAX_HOP & 0xff);

    // Source Address
    buffer[PREAMBLE_LEN + 5] = (unsigned char) ((hopCount >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 6] = (unsigned char) ((hopCount >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 7] = (unsigned char) ((hopCount >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 8] = (unsigned char) (hopCount & 0xff);

    // Unreacheable Address
    buffer[PREAMBLE_LEN + 9] = (unsigned char) ((self_ip >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 10] = (unsigned char) ((self_ip >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 11] = (unsigned char) ((self_ip >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 12] = (unsigned char) (self_ip & 0xff);
    
    // Unreacheable Address
    buffer[PREAMBLE_LEN + 13] = (unsigned char) ((ip_unr >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 14] = (unsigned char) ((ip_unr >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 15] = (unsigned char) ((ip_unr >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 16] = (unsigned char) (ip_unr & 0xff);
    
    
}

static void construct_rreq_wTrail(char* buffer, int *ips, int ip_n, int src, int dest, int seq_n, int hopC, int maxHop) {
    
    int i, c;
    hopC++;    
    
    //msg-hop-limit    
    buffer[PREAMBLE_LEN + 1] = (unsigned char) ((maxHop >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 2] = (unsigned char) ((maxHop >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 3] = (unsigned char) ((maxHop >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 4] = (unsigned char) (maxHop & 0xff);     
    
    // Source Address
    buffer[PREAMBLE_LEN + 5] = (unsigned char) ((hopC >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 6] = (unsigned char) ((hopC >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 7] = (unsigned char) ((hopC >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 8] = (unsigned char) (hopC & 0xff);

    // Destination
    buffer[PREAMBLE_LEN + 9] = (unsigned char) ((src >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 10] = (unsigned char) ((src >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 11] = (unsigned char) ((src >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 12] = (unsigned char) (src & 0xff);
    
    //Length of addr
    buffer[PREAMBLE_LEN + 13] = (unsigned char) ((dest >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 14] = (unsigned char) ((dest >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 15] = (unsigned char) ((dest >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 16] = (unsigned char) (dest & 0xff);
    
    //Length of addr
    buffer[PREAMBLE_LEN + 17] = (unsigned char) ((seq_n >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 18] = (unsigned char) ((seq_n >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 19] = (unsigned char) ((seq_n >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 20] = (unsigned char) (seq_n & 0xff);
    
    //Length of addr
    buffer[PREAMBLE_LEN + 21] = (unsigned char) ((ip_n >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 22] = (unsigned char) ((ip_n >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 23] = (unsigned char) ((ip_n >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 24] = (unsigned char) (ip_n & 0xff);
    
    c = 0;
    for (i = 25; i < (25+ip_n*4); i = i+4){
        buffer[PREAMBLE_LEN + i] = (unsigned char) ((ips[c] >> 24) & 0xff);
        buffer[PREAMBLE_LEN + i+1] = (unsigned char) ((ips[c] >> 16) & 0xff);
        buffer[PREAMBLE_LEN + i+2] = (unsigned char) ((ips[c] >> 8) & 0xff);
        buffer[PREAMBLE_LEN + i+3] = (unsigned char) (ips[c] & 0xff);
        c++;
    }

}

static void construct_rrep_wTrail(char* buffer, int *ips, int ip_n, int src, int dest, int seq_n, int hopC, int maxHop) {
    
    int i, c;
    hopC++;
    
    
     //msg-hop-limit    
    buffer[PREAMBLE_LEN + 1] = (unsigned char) ((maxHop >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 2] = (unsigned char) ((maxHop >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 3] = (unsigned char) ((maxHop >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 4] = (unsigned char) (maxHop & 0xff);       
    
    
    // Source Address
    buffer[PREAMBLE_LEN + 5] = (unsigned char) ((hopC >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 6] = (unsigned char) ((hopC >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 7] = (unsigned char) ((hopC >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 8] = (unsigned char) (hopC & 0xff);

    
    buffer[PREAMBLE_LEN + 9] = (unsigned char) ((src >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 10] = (unsigned char) ((src >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 11] = (unsigned char) ((src >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 12] = (unsigned char) (src & 0xff);

    //Length of addr
    buffer[PREAMBLE_LEN + 13] = (unsigned char) ((dest >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 14] = (unsigned char) ((dest >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 15] = (unsigned char) ((dest >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 16] = (unsigned char) (dest & 0xff);
    
    //Length of addr
    buffer[PREAMBLE_LEN + 17] = (unsigned char) ((seq_n >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 18] = (unsigned char) ((seq_n >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 19] = (unsigned char) ((seq_n >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 20] = (unsigned char) (seq_n & 0xff); 
    
    //Length of addr
    buffer[PREAMBLE_LEN + 21] = (unsigned char) ((ip_n >> 24) & 0xff);
    buffer[PREAMBLE_LEN + 22] = (unsigned char) ((ip_n >> 16) & 0xff);
    buffer[PREAMBLE_LEN + 23] = (unsigned char) ((ip_n >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 24] = (unsigned char) (ip_n & 0xff);   
    
    c = 0;
    for (i = 25; i < (25+ip_n*4); i = i+4){
        buffer[PREAMBLE_LEN + i] = (unsigned char) ((ips[c] >> 24) & 0xff);
        buffer[PREAMBLE_LEN + i+1] = (unsigned char) ((ips[c] >> 16) & 0xff);
        buffer[PREAMBLE_LEN + i+2] = (unsigned char) ((ips[c] >> 8) & 0xff);
        buffer[PREAMBLE_LEN + i+3] = (unsigned char) (ips[c] & 0xff);
        c++;
    }

}

static void construct_ack(char* buffer, int dest) {
    int i;
    for (i = 0; i < PREAMBLE_LEN; i++)
        buffer[i] = 0xaa; // Preamble
    // SFD
    buffer[PREAMBLE_LEN] = 0x3a;
 
    // Destination id
    buffer[PREAMBLE_LEN + 1] = (unsigned char) ((dest >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 2] = (unsigned char) (dest & 0xff);
    
}



static void construct_frame_header(char* buffer, int dest_ip, int payload_len, char sfd) {
    int i, value = 0;

    for (i = 0; i < PREAMBLE_LEN; i++)
        buffer[i] = 0xaa; // Preamble
    
    // SFD
    //0xa3 - data
    //0xa9 - rreq
    buffer[PREAMBLE_LEN] = sfd;
    
    // Length of payload
    buffer[PREAMBLE_LEN + 1] = (unsigned char) ((payload_len >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 2] = (unsigned char) (payload_len & 0xff);
    
    if(sfd==0x9a || sfd==0xa8){        
        value = broadcast_id;
    } else {        
        value = getIdFromArpTable(get_next_hop_from_routingTable(dest_ip));
    }
    
    if(value==0){
//        printk("-- No ID for the following IP: ");print_ip(dest_ip);printk("\n");
        value = 1;
    } 
//    else if(value!=255){
//        printk("-- Found ID %d for the following IP: ", value); print_ip(dest_ip);printk("\n");
//    } else {
//        printk("-- Using broadcast ID (255) for the following IP: "); print_ip(dest_ip);printk("\n");
//    }
    
    // Destination id
    buffer[PREAMBLE_LEN + 3] = (unsigned char) ((value >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 4] = (unsigned char) (value & 0xff);

    // Source id
    buffer[PREAMBLE_LEN + 5] = (unsigned char) ((self_id >> 8) & 0xff);
    buffer[PREAMBLE_LEN + 6] = (unsigned char) (self_id & 0xff);


}

static void generate_DATA_frame(struct vlc_packet *pkt) {
    char sfd = 0xa3;
    int ip;
    int i, payload_len, index_block, encoded_len, num_of_blocks = 0;
    payload_len = pkt->datalen - (MAC_HDR_LEN - OCTET_LEN);
    encoded_len = payload_len + 2 * MAC_ADDR_LEN + PROTOCOL_LEN;
    // Calculate the number of blocks
    if (encoded_len % block_size)
        num_of_blocks = encoded_len / block_size + 1;
    else
        num_of_blocks = encoded_len / block_size;

    data_buffer_byte_len = FRAME_LEN_WO_PAYLOAD + payload_len + ECC_LEN * (num_of_blocks - 1);
    //data_buffer_byte = kmalloc(data_buffer_byte_len, GFP_KERNEL);
    memset(data_buffer_byte, 0, sizeof (unsigned char) * data_buffer_byte_len);
    data_buffer_symbol_len = (data_buffer_byte_len - PREAMBLE_LEN)*8 * 2
            + PREAMBLE_LEN * 8 + 1; // Send a BIT more, why? -- Avoid phy error
    //data_buffer_symbol =
    //kmalloc((data_buffer_symbol_len)*sizeof(_Bool), GFP_KERNEL);
    if (data_buffer_byte == NULL || data_buffer_symbol == NULL) {
        printk("Ran out of memory generating new frames.\n");
        return;
    }
    // Construct a new data frame
    memcpy(data_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN, pkt->data,
            pkt->datalen); // Copy the payload
    
    ip = pkt->destination;
    vlc_release_buffer(pkt); // Return the buffer to the pool
    
    construct_frame_header(data_buffer_byte, pkt->destination, payload_len, sfd);


    //print_uncoded_frame();
    /// Encode the blocks of a frame
    for (index_block = 0; index_block < num_of_blocks; index_block++) {
        for (i = 0; i < ECC_LEN; i++)
            par[i] = 0;
        if (index_block < num_of_blocks - 1) {
            encode_rs8(rs_decoder,
                    data_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN + index_block*block_size,
                    block_size, par, 0);
        } else {
            encode_rs8(rs_decoder,
                    data_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN + index_block*block_size,
                    encoded_len % block_size, par, 0);
        }
        for (i = 0; i < ECC_LEN; i++)
            data_buffer_byte[FRAME_LEN_WO_PAYLOAD + payload_len + (index_block - 1) * ECC_LEN + i] = par[i];
    }

    // Encoding the frame
    OOK_with_Manchester_RLL(data_buffer_byte, data_buffer_symbol,
            data_buffer_byte_len);
    tx_data_curr_index = data_buffer_symbol_len;
    // print_uncoded_frame();
    //if (data_buffer_byte) kfree(data_buffer_byte);
    //    print_coded_frame();
}

static void generate_ACK_frame(int dest) {
    //ack_buffer_byte_len = FRAME_LEN_WO_PAYLOAD;
    ack_buffer_byte_len = PREAMBLE_LEN + SFD_LEN + MAC_ADDR_LEN + 10;
    //ack_buffer_byte = kmalloc(ack_buffer_byte_len, GFP_KERNEL);
    ack_buffer_symbol_len = (ack_buffer_byte_len - PREAMBLE_LEN)*8 * 2
            + PREAMBLE_LEN * 8 + 1; // Send a BIT more, why? -- Avoid phy error
    //ack_buffer_symbol =
    //kmalloc(ack_buffer_symbol_len*sizeof(_Bool), GFP_KERNEL);
    if (ack_buffer_byte == NULL || ack_buffer_symbol == NULL) {
        printk("Ran out of memory generating new frames.\n");
        return;
    }
    // Construct a new frame
    //construct_frame(ack_buffer_byte, ack_buffer_byte_len, 0);
    construct_ack(ack_buffer_byte, dest);
    // Encoding the frame
    OOK_with_Manchester_RLL(ack_buffer_byte, ack_buffer_symbol,
            ack_buffer_byte_len);
    //print_uncoded_ack();
    //if (ack_buffer_byte) kfree(ack_buffer_byte);
    //    print_coded_frame();
}

static void generate_reqId_frame(void) {
    //ack_buffer_byte_len = FRAME_LEN_WO_PAYLOAD;
    reqId_buffer_byte_len = PREAMBLE_LEN + SFD_LEN + 1 * IP_ADDR_LEN + 2 * MAC_ADDR_LEN + 10;
    //ack_buffer_byte = kmalloc(ack_buffer_byte_len, GFP_KERNEL);
    reqId_buffer_symbol_len = (reqId_buffer_byte_len - PREAMBLE_LEN)*8 * 2
            + PREAMBLE_LEN * 8 + 1; // Send a BIT more, why? -- Avoid phy error
    //ack_buffer_symbol =
    //kmalloc(ack_buffer_symbol_len*sizeof(_Bool), GFP_KERNEL);
    if (reqid_buffer_byte == NULL || reqId_buffer_symbol == NULL) {
        printk("Ran out of memory generating new frames.\n");
        return;
    }
    // Construct a new frame
    //construct_frame(ack_buffer_byte, ack_buffer_byte_len, 0);
    construct_reqId(reqid_buffer_byte);
    // Encoding the frame
    OOK_with_Manchester_RLL(reqid_buffer_byte, reqId_buffer_symbol,
            reqId_buffer_byte_len);
    //print_uncoded_ack();
    //if (ack_buffer_byte) kfree(ack_buffer_byte);
    //    print_coded_frame();
    tx_reqId_curr_index = reqId_buffer_symbol_len;
}

static void generate_RepId_frame(int source) {
    //ack_buffer_byte_len = FRAME_LEN_WO_PAYLOAD;
    repId_buffer_byte_len = PREAMBLE_LEN + SFD_LEN +  1 * IP_ADDR_LEN + 2 * MAC_ADDR_LEN + 10;
    //ack_buffer_byte = kmalloc(ack_buffer_byte_len, GFP_KERNEL);
    repId_buffer_symbol_len = (repId_buffer_byte_len - PREAMBLE_LEN)*8 * 2
            + PREAMBLE_LEN * 8 + 1; // Send a BIT more, why? -- Avoid phy error
    //ack_buffer_symbol =
    //kmalloc(ack_buffer_symbol_len*sizeof(_Bool), GFP_KERNEL);
    if (repId_buffer_byte == NULL || repId_buffer_symbol == NULL) {
        printk("Ran out of memory generating new frames.\n");
        return;
    }
    // Construct a new frame
    //construct_frame(ack_buffer_byte, ack_buffer_byte_len, 0);
    construct_repId(repId_buffer_byte, source);
    // Encoding the frame
    OOK_with_Manchester_RLL(repId_buffer_byte, repId_buffer_symbol,
            repId_buffer_byte_len);
    //print_uncoded_ack();
    //if (ack_buffer_byte) kfree(ack_buffer_byte);

    tx_repId_curr_index = repId_buffer_symbol_len;
}

static void generate_Rreq_frame(int ip_dest){

    int i, payload_len, index_block, encoded_len, num_of_blocks = 0;
    int rreq_length = 0;
    rreq_length = 6 * IP_ADDR_LEN + 10;    
    memset(rreq_buffer_byte, 0, sizeof (unsigned char) * rreq_length);
    construct_rreq(rreq_buffer_byte, ip_dest);
    memcpy(tmp_pkt->data, rreq_buffer_byte, rreq_length);
    tmp_pkt->datalen = rreq_length;

    
    payload_len = tmp_pkt->datalen - (MAC_HDR_LEN - OCTET_LEN);

    encoded_len = payload_len + 2 * MAC_ADDR_LEN + PROTOCOL_LEN;
    // Calculate the number of blocks
    if (encoded_len % block_size)
        num_of_blocks = encoded_len / block_size + 1;
    else
        num_of_blocks = encoded_len / block_size;

    rreq_buffer_byte_len = FRAME_LEN_WO_PAYLOAD + payload_len + ECC_LEN * (num_of_blocks - 1);

    memset(rreq_buffer_byte, 0, sizeof (unsigned char) * rreq_buffer_byte_len);
    rreq_buffer_symbol_len = (rreq_buffer_byte_len - PREAMBLE_LEN)*8 * 2
            + PREAMBLE_LEN * 8 + 1; // Send a BIT more, why? -- Avoid phy error

    if (rreq_buffer_byte == NULL || rreq_buffer_symbol == NULL) {
        printk("Ran out of memory generating new frames.\n");
        return;
    }
    // Construct a new data frame
    memcpy(rreq_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN, tmp_pkt->data,
            tmp_pkt->datalen); // Copy the payload
    construct_frame_header(rreq_buffer_byte, ip_dest, payload_len, 0x9a);


    //print_uncoded_frame();
    /// Encode the blocks of a frame
    for (index_block = 0; index_block < num_of_blocks; index_block++) {
        for (i = 0; i < ECC_LEN; i++)
            par[i] = 0;
        if (index_block < num_of_blocks - 1) {
            encode_rs8(rs_decoder,
                    rreq_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN + index_block*block_size,
                    block_size, par, 0);
        } else {
            encode_rs8(rs_decoder,
                    rreq_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN + index_block*block_size,
                    encoded_len % block_size, par, 0);
        }
        for (i = 0; i < ECC_LEN; i++)
            rreq_buffer_byte[FRAME_LEN_WO_PAYLOAD + payload_len + (index_block - 1) * ECC_LEN + i] = par[i];
    }

    // Encoding the frame
    OOK_with_Manchester_RLL(rreq_buffer_byte, rreq_buffer_symbol,
            rreq_buffer_byte_len);
    tx_rreq_curr_index = rreq_buffer_symbol_len;
}

static void generate_Rerr_frame(int ip_dest){

    int i, payload_len, index_block, encoded_len, num_of_blocks = 0;
    int rerr_length = 0;
    rerr_length = 4 * IP_ADDR_LEN + 10;    
    memset(rerr_buffer_byte, 0, sizeof (unsigned char) * rerr_length);
    construct_rerr(rerr_buffer_byte, ip_dest);
    memcpy(tmp_pkt->data, rerr_buffer_byte, rerr_length);
    tmp_pkt->datalen = rerr_length;

    
    payload_len = tmp_pkt->datalen - (MAC_HDR_LEN - OCTET_LEN);
//    printk("Payload length: %d\n", payload_len);

    encoded_len = payload_len + 2 * MAC_ADDR_LEN + PROTOCOL_LEN;
    // Calculate the number of blocks
    if (encoded_len % block_size)
        num_of_blocks = encoded_len / block_size + 1;
    else
        num_of_blocks = encoded_len / block_size;

    rerr_buffer_byte_len = FRAME_LEN_WO_PAYLOAD + payload_len + ECC_LEN * (num_of_blocks - 1);

    memset(rerr_buffer_byte, 0, sizeof (unsigned char) * rerr_buffer_byte_len);
    rerr_buffer_symbol_len = (rerr_buffer_byte_len - PREAMBLE_LEN)*8 * 2
            + PREAMBLE_LEN * 8 + 1; // Send a BIT more, why? -- Avoid phy error

    if (rerr_buffer_byte == NULL || rerr_buffer_symbol == NULL) {
        printk("Ran out of memory generating new frames.\n");
        return;
    }
    // Construct a new data frame
    memcpy(rerr_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN, tmp_pkt->data,
            tmp_pkt->datalen); // Copy the payload
    construct_frame_header(rerr_buffer_byte, 0, payload_len, 0xa8);


    //print_uncoded_frame();
    /// Encode the blocks of a frame
    for (index_block = 0; index_block < num_of_blocks; index_block++) {
        for (i = 0; i < ECC_LEN; i++)
            par[i] = 0;
        if (index_block < num_of_blocks - 1) {
            encode_rs8(rs_decoder,
                    rerr_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN + index_block*block_size,
                    block_size, par, 0);
        } else {
            encode_rs8(rs_decoder,
                    rerr_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN + index_block*block_size,
                    encoded_len % block_size, par, 0);
        }
        for (i = 0; i < ECC_LEN; i++)
            rerr_buffer_byte[FRAME_LEN_WO_PAYLOAD + payload_len + (index_block - 1) * ECC_LEN + i] = par[i];
    }

    // Encoding the frame
    OOK_with_Manchester_RLL(rerr_buffer_byte, rerr_buffer_symbol,
            rerr_buffer_byte_len);
    tx_rerr_curr_index = rerr_buffer_symbol_len;
}

static void generate_Rreq_frame_wTrail(int source, int dest, int ip_n, int *ips, int seq_n, int hopC, int max_hop){
    
    int i, payload_len, index_block, encoded_len, num_of_blocks = 0;
//    printk("| ");
//    printk(" %d ", hopC+1);printk(" | ");
//    print_ip(source);printk(" | ");
//    print_ip(dest);printk(" | ");
//    printk(" %d ", seq_n);printk(" | ");
//    printk(" %d ", ip_n);printk(" | ");
//    for(i=0;i<ip_n;i++){
//        print_ip(ips[i]);
//        printk(" | ");
//    }
//    printk("\n");
    
    rreq_buffer_byte_len = (6+ip_n) * IP_ADDR_LEN + 10;
    memset(rreq_buffer_byte, 0, sizeof (unsigned char) * rreq_buffer_byte_len);
    memset(tmp_pkt->data, 0, sizeof (unsigned char) * rreq_buffer_byte_len);

    construct_rreq_wTrail(rreq_buffer_byte, ips, ip_n, source, dest, seq_n, hopC, max_hop);

    memcpy(tmp_pkt->data, rreq_buffer_byte, rreq_buffer_byte_len);
    tmp_pkt->datalen = rreq_buffer_byte_len;

    payload_len = tmp_pkt->datalen - (MAC_HDR_LEN - OCTET_LEN);
//    printk("Payload length: %d\n", payload_len);

    encoded_len = payload_len + 2 * MAC_ADDR_LEN + PROTOCOL_LEN;
    // Calculate the number of blocks
    if (encoded_len % block_size)
        num_of_blocks = encoded_len / block_size + 1;
    else
        num_of_blocks = encoded_len / block_size;

    rreq_buffer_byte_len = FRAME_LEN_WO_PAYLOAD + payload_len + ECC_LEN * (num_of_blocks - 1);
    //data_buffer_byte = kmalloc(data_buffer_byte_len, GFP_KERNEL);
    memset(rreq_buffer_byte, 0, sizeof (unsigned char) * rreq_buffer_byte_len);

    rreq_buffer_symbol_len = (rreq_buffer_byte_len - PREAMBLE_LEN)*8 * 2
            + PREAMBLE_LEN * 8 + 1; // Send a BIT more, why? -- Avoid phy error
    //data_buffer_symbol =
    //kmalloc((data_buffer_symbol_len)*sizeof(_Bool), GFP_KERNEL);
    
    if (rreq_buffer_byte == NULL || rreq_buffer_symbol == NULL) {
        printk("Ran out of memory generating new frames.\n");
        return;
    }    
    
    // Construct a new data frame
    memcpy(rreq_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN, tmp_pkt->data,
            tmp_pkt->datalen); // Copy the payload
    construct_frame_header(rreq_buffer_byte,dest , payload_len, 0x9a);


    //print_uncoded_frame();
    /// Encode the blocks of a frame
    for (index_block = 0; index_block < num_of_blocks; index_block++) {
        for (i = 0; i < ECC_LEN; i++)
            par[i] = 0;
        if (index_block < num_of_blocks - 1) {
            encode_rs8(rs_decoder,
                    rreq_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN + index_block*block_size,
                    block_size, par, 0);
        } else {
            encode_rs8(rs_decoder,
                    rreq_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN + index_block*block_size,
                    encoded_len % block_size, par, 0);
        }
        for (i = 0; i < ECC_LEN; i++)
            rreq_buffer_byte[FRAME_LEN_WO_PAYLOAD + payload_len + (index_block - 1) * ECC_LEN + i] = par[i];
    }
    
    
    // Encoding the frame
    OOK_with_Manchester_RLL(rreq_buffer_byte, rreq_buffer_symbol,
            rreq_buffer_byte_len);

    tx_rreq_curr_index = rreq_buffer_symbol_len;
}

static void generate_Rrep_frame_wTrail(int source, int dest, int ip_n, int *ips, int seq_n, int hopC, int max_hop){
        
    int i, payload_len, index_block, encoded_len, num_of_blocks = 0;
    printk("| ");
    printk(" %d ", hopC+1);printk(" | ");
    print_ip(source);printk(" | ");
    print_ip(dest);printk(" | ");
    printk(" %d ", seq_n);printk(" | ");
    printk(" %d ", ip_n);printk(" | ");
    for(i=0;i<ip_n;i++){
        print_ip(ips[i]);
        printk(" | ");
    }
    printk("\n");
    
    rrep_buffer_byte_len = (6+ip_n) * IP_ADDR_LEN + 10;
    memset(rrep_buffer_byte, 0, sizeof (unsigned char) * rrep_buffer_byte_len);
    memset(tmp_pkt->data, 0, sizeof (unsigned char) * rrep_buffer_byte_len);

    construct_rrep_wTrail(rrep_buffer_byte, ips, ip_n, source, dest, seq_n, hopC, max_hop);

    memcpy(tmp_pkt->data, rrep_buffer_byte, rrep_buffer_byte_len);
    tmp_pkt->datalen = rrep_buffer_byte_len;

    payload_len = tmp_pkt->datalen - (MAC_HDR_LEN - OCTET_LEN);
//    printk("Payload length: %d\n", payload_len);

    encoded_len = payload_len + 2 * MAC_ADDR_LEN + PROTOCOL_LEN;
    // Calculate the number of blocks
    if (encoded_len % block_size)
        num_of_blocks = encoded_len / block_size + 1;
    else
        num_of_blocks = encoded_len / block_size;

    rrep_buffer_byte_len = FRAME_LEN_WO_PAYLOAD + payload_len + ECC_LEN * (num_of_blocks - 1);
    //data_buffer_byte = kmalloc(data_buffer_byte_len, GFP_KERNEL);
    memset(rrep_buffer_byte, 0, sizeof (unsigned char) * rrep_buffer_byte_len);

    rrep_buffer_symbol_len = (rrep_buffer_byte_len - PREAMBLE_LEN)*8 * 2
            + PREAMBLE_LEN * 8 + 1; // Send a BIT more, why? -- Avoid phy error
    //data_buffer_symbol =
    //kmalloc((data_buffer_symbol_len)*sizeof(_Bool), GFP_KERNEL);
    
    if (rrep_buffer_byte == NULL || rrep_buffer_symbol == NULL) {
        printk("Ran out of memory generating new frames.\n");
        return;
    }    
    
    // Construct a new data frame
    memcpy(rrep_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN, tmp_pkt->data,
            tmp_pkt->datalen); // Copy the payload
    construct_frame_header(rrep_buffer_byte, dest, payload_len, 0xa9);

    //print_uncoded_frame();
    /// Encode the blocks of a frame
    for (index_block = 0; index_block < num_of_blocks; index_block++) {
        for (i = 0; i < ECC_LEN; i++)
            par[i] = 0;
        if (index_block < num_of_blocks - 1) {
            encode_rs8(rs_decoder,
                    rrep_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN + index_block*block_size,
                    block_size, par, 0);
        } else {
            encode_rs8(rs_decoder,
                    rrep_buffer_byte + PREAMBLE_LEN + SFD_LEN + OCTET_LEN + index_block*block_size,
                    encoded_len % block_size, par, 0);
        }
        for (i = 0; i < ECC_LEN; i++)
            rrep_buffer_byte[FRAME_LEN_WO_PAYLOAD + payload_len + (index_block - 1) * ECC_LEN + i] = par[i];
    }
    
    
    // Encoding the frame
    OOK_with_Manchester_RLL(rrep_buffer_byte, rrep_buffer_symbol,
            rrep_buffer_byte_len);

    tx_rrep_curr_index = rrep_buffer_symbol_len;
}


static int basic_sensing(_Bool f_display) {
    int cnt_symbol = 0;
    int i = 0;
    int timeout = 1;
    index_long_sensing = 0;
    phy_state = SENSING;

    while (index_long_sensing < LONG_SENSING_SYMBOLS && timeout < 100) {
        msleep(1);
        ++timeout;
    }
    if (timeout >= 100) {
        printk("Sensing timeout %d.\n", phy_state);
        return 0;
    }
    if (f_display) printk("sensing: %d ", sensing_buffer[0]);
    avg_val_from_sensing = sensing_buffer[0];
    for (i = 1; i < LONG_SENSING_SYMBOLS; i++) {
        if (f_display) printk("%d ", sensing_buffer[i]);
        avg_val_from_sensing += sensing_buffer[i];
        if (abs(sensing_buffer[i] - sensing_buffer[i - 1]) > min_threshold)
            ++cnt_symbol;
    }
    avg_val_from_sensing /= LONG_SENSING_SYMBOLS;
    if (f_display) printk("\n");
    return cnt_symbol;
}




static int mac_tx(void *data) {
    uint32_t addr;
    _Bool isInRoutingTable = false;
    int jStartU, jEndU, timeCount, hopcount;
    int backoff_timer = 0;
    int diff = 0;
    struct vlc_priv *priv = netdev_priv(vlc_devs);    
    // A small sleep in order to have device correct IP
    jStart = jiffies;
    jStartU = jiffies_to_msecs(jStart);
    
    //Wait one second before starting in order to be able to get device's IP
    msleep(decoding_sleep_slot * 1000);
    
    
    jEnd = jiffies;
    jEndU = jiffies_to_msecs(jEnd);              
    diff = jEndU - jStartU;
    
    printk("\n[Diff: %d ms]\n", diff);
    
    struct timeval;
    addr = inet_select_addr(vlc_devs, 0, RT_SCOPE_UNIVERSE);
    self_ip = addr;  
    
    
    
    // Fill some entries in the arp table
    table[free_position].ip_address = 100706496;
    table[free_position].mac_addr = 6;    
    free_position++;
    table[free_position].ip_address = 134260928;
    table[free_position].mac_addr = 8;
    free_position++;
    table[free_position].ip_address = 151038144;
    table[free_position].mac_addr = 9;
    free_position++;
    table[free_position].ip_address = 117483712;
    table[free_position].mac_addr = 7;
    free_position++;
    
    printk("Node IP: "); print_ip(self_ip);printk("\n");
    
    
start:
    for (;;) {
        

        // Every time a node is added to the network, it immediately sends a REQ_ID to its neighbors.
//        if (setup_phase){
//            printk("Setup phase: identify neighbors..\n");
//            generate_reqId_frame();
//            tx_reqId_curr_index = 0;
//            f_ready_to_tx_reqId = 1;
//            phy_state = TX;
//            setup_phase = 0;
//            msleep(10*decoding_sleep_slot);            
//        }
        
//        build_routing_table = 0;
//        if (build_routing_table) {
//            printk("Start building route\n");
//            generate_Rreq_frame();
//            tx_rreq_curr_index = 0;
//            f_ready_to_tx_rreq = 1;
//            phy_state = TX;
//
//
//            for (;;) {
//                if (f_rreq_timeout) {
//                    f_rreq_timeout = false;
//                    build_routing_table = false;
//                    msleep(decoding_sleep_slot);
//                    break;
//                } else {
//                    msleep(decoding_sleep_slot);
//                    continue;
//                }
//            }
//
//        }        
        
        if (mac_or_app == APP) {
            if (!priv->tx_queue) {
                msleep(decoding_sleep_slot);
                if (kthread_should_stop()) {
                    printk("\n=======EXIT mac_tx=======\n");
                    return 0;
                }
                goto start;
            }
            tx_pkt = vlc_dequeue_pkt(vlc_devs);
            printk("Dequeue a packet to dest: ");
            print_ip(tx_pkt->destination); printk("\n");
            
            /// Create a frame with the dequeued packet
            generate_DATA_frame(tx_pkt); // Generate MAC frame
            
            //Check if IP is in routing table
            isInRoutingTable = checkIfNodeInRoutingTable(tx_pkt->destination);
            if(isInRoutingTable){
                //If node is in routing table, just send the packet. The frame will be built based on the destiantion address from arp table
                printk("Node "); print_ip(tx_pkt->destination);printk(" is in routing table.\n"); 
                
            } else {                    
                jStart = jiffies;
                jStartU = jiffies_to_msecs(jStart);
                
                if(self_ip != tx_pkt->source){
//                    printk("Break route to node and send RERR.. \n");
                    generate_Rerr_frame(tx_pkt->destination);          
                    
                    get_random_bytes(&backoff_timer, sizeof (backoff_timer));
                    CW = CW_min;
                    backoff_timer = abs(backoff_timer) % CW + 1;
                
                    wait_rerr1: // waiting for ACK || receiving || transmitting || busy channel
                    while ((phy_state == TX) || basic_sensing(false)) {
                        msleep(decoding_sleep_slot);
                        if (kthread_should_stop()) {
                            printk("\n=======EXIT mac_tx=======\n");
                            return 0;
                        }
                    }
                    backoff_timer -= 1;
                    while (backoff_timer)
                        goto wait_rerr1;

                    tx_rerr_curr_index = 0;
                    f_ready_to_tx_rerr = 1;
                    phy_state = TX;  
                    printk("Sending RERR.. \n");
                    goto start;
                }
                
            wait_for_repp:              
                generate_Rreq_frame(tx_pkt->destination);
                tx_rreq_curr_index = 0;
                f_ready_to_tx_rreq = 1;
                phy_state = TX;
                
                
                for (;;) {
                    if (f_rreq_timeout) {
                        
                        jEnd = jiffies;
                        jEndU = jiffies_to_msecs(jEnd);                        
                        diff = jEndU - jStartU;
//                        printk("[Finished building route - Diff: %d ms]\n", diff);
                        
                        f_rreq_timeout_count++;
                        f_rreq_timeout = false;
                        build_routing_table = false;
                        msleep(decoding_sleep_slot);  
                        if(f_rreq_timeout_count < DISCOVERY_ATTEMPTS_MAX ){                             
                            jStart = jiffies;
                            jStartU = jiffies_to_msecs(jStart);
                            goto wait_for_repp;
                        } else {
                            jStart = jiffies;
                            jStartU = jiffies_to_msecs(jStart);
                            timeCount = 0;
                            //Wait for holddown time until trying new route
                            msleep(RREQ_HOLDDOWN_TIME);  
                            jEnd = jiffies;
                            jEndU = jiffies_to_msecs(jEnd);              
                            diff = jEndU - jStartU;
                            f_rreq_timeout_count = 0;        
                            route_discovery_fails++;
                            route_discovery_attempts++;
                            printk("Fail Route Discovery - Diff: %d ms - N of Fails:%d\n", diff, route_discovery_fails);
                            printk("RREQ Attempts: %d\n", route_discovery_attempts);  
                            msleep(decoding_sleep_slot);
                            goto start; 
                        }
                    } else if(f_received_rrep){
                        if(jStartU_global != 0){
                            jEnd = jiffies;
                            jEndU_global = jiffies_to_msecs(jEnd);                        
                            diff = jEndU_global - jStartU_global;
                            printk("Diff between RERR and new route: %d ms\n", diff);
                            jStartU_global = 0;
                        }
                        route_discovery_attempts++;
                        route_discovery_success++;
                        printk("RREQ Attempts: %d\n", route_discovery_attempts);  
                        jEnd = jiffies;
                        jEndU = jiffies_to_msecs(jEnd);                        
                        diff = jEndU - jStartU;
                        printk("Success Route Discovery - Diff: %d ms - N of successes:%d\n", diff, route_discovery_success);
                        
                        f_received_rrep = 0;
                        f_rreq_timeout = false;
                        f_rreq_timeout_count = 0;
                        goto start;
                    } else{
                        msleep(decoding_sleep_slot);
                    }
                }
            }

        }

        for (;;) {
            // If I received the ACK or dropped the frame, break from this loop. In other words:
            // stop trying to transmit this frame
            
            if (f_received_ack) {
                if(get_hop_count_from_routingTable(tx_pkt->destination)>1){                    
                    msleep(400*decoding_sleep_slot);
                } else {
                    msleep(decoding_sleep_slot);
                }
                max_fails = 0;
                f_received_ack = false;
                break;
            }
            
            
            //The control will be here if we are using reference counter from MAC layer
            if(f_dropped_frame){
                f_dropped_frame = false;
                
                if(staticRoute){
                    msleep(decoding_sleep_slot);
                    break;
                }
                
                if(max_fails < MAX_DROP){
                    max_fails++;             
                    msleep(10*decoding_sleep_slot);
                    break;
                }
                mac_broken_link++;
                printk("MAC_broken msgs: %d\n", mac_broken_link);  
                max_fails = 0;
                breakRouteToNode(tx_pkt->destination);

                //If i'm forwarding and dropped packet, send RERR back in the route. 
                //If i'm source, try rreq again
                if(self_ip != tx_pkt->source){
//                    printk("Break route to node and send RERR.. \n");
                    generate_Rerr_frame(tx_pkt->destination);          
                    
                    get_random_bytes(&backoff_timer, sizeof (backoff_timer));
                    CW = CW_min;
                    backoff_timer = abs(backoff_timer) % CW + 1;
                
                    wait_rerr2: // waiting for ACK || receiving || transmitting || busy channel
                    while ((phy_state == TX) || basic_sensing(false)) {
                        msleep(decoding_sleep_slot);
                        if (kthread_should_stop()) {
                            printk("\n=======EXIT mac_tx=======\n");
                            return 0;
                        }
                    }
                    backoff_timer -= 1;
                    while (backoff_timer)
                        goto wait_rerr2;

                    tx_rerr_curr_index = 0;
                    f_ready_to_tx_rerr = 1;
                    phy_state = TX;   
                    break;
                }

                msleep(decoding_sleep_slot);
                break;
            }

            
            //If we are trying to send the data but the phy_state is TX, it meanse
            //the ACK is being transmitted.. so lets wait for a decoding slot 
            // and try again
            if (phy_state == TX) { // This node is  transmitting the ACK
                msleep(decoding_sleep_slot);
                //printk("phy_state == TX %d\n", phy_state);
                continue;
            }
            

            //If we got to this point, it means that we haven't received the ack, we haven't dropped
            //the frame and phy_state is not TX. In other words, we are able to transmit.
            //However, we have to check if we are waiting for the ack and if the index is inside boundaries...
            if (!f_wait_for_ack && tx_data_curr_index >= data_buffer_symbol_len) {

                if (!f_re_tx) {
                    //We are not trying to retransmit the frame. Let's set the backoff timer to send data!
                    // printk("Transmit frame \n");
                    f_ready_to_tx = 0;
                    get_random_bytes(&backoff_timer, sizeof (backoff_timer));
                    CW = CW_min;
                    backoff_timer = abs(backoff_timer) % CW + 1;
                } else {
                    // Retransmit a frame
                    // printk("Retransmit frame \n");
                    CW = CW << 1;
                    CW = (CW < CW_max) ? CW : CW_max;
                    backoff_timer = (abs(backoff_timer) % CW + 1)*backoff_extra;
                    f_re_tx = false;
                }
wait: // waiting for ACK || receiving || transmitting || busy channel
                while ((phy_state == TX) || basic_sensing(false)) {
                    //if our phy_state is TX, it means we are transmitting the ACK frame..
                    //Basic sensing is the method we call in order to 
                    //sense the channel and check if we are able to transmit... if it
                    //returns true, it means the channel is busy 
                    //printk("Basic sensing...\n");
                    msleep(decoding_sleep_slot);
                    if (kthread_should_stop()) {
                        printk("\n=======EXIT mac_tx=======\n");
                        return 0;
                    }
                }
                backoff_timer -= 1;
                //backoff timer... goes down until we are sure the channel is free..
                while (backoff_timer)
                    goto wait;
                //From this point on, we are actually transmitting our data..

                tx_data_curr_index = 0;
                f_ready_to_tx = 1;

                if (!hpl) switch_led_to_tx();
                phy_state = TX; // Switch to TX mode to transmit the data frame
            }
            msleep(decoding_sleep_slot);
        }
        if (kthread_should_stop()) {
            printk("\n=======EXIT mac_tx=======\n");
            return 0;
        }
    }

}

/*
 * Receive a packet: retrieve, encapsulate and pass over to upper levels
 */
void mac_rx(struct net_device *dev, struct vlc_packet *pkt) {
    unsigned int addr, Daddr;
    struct sk_buff *skb;
    struct sk_buff *skb_tmp;
    struct vlc_priv *priv = netdev_priv(dev);
    struct iphdr *iph;    
    unsigned char bytes[4];
    
    
    skb = dev_alloc_skb(pkt->datalen + 2);
    if (!skb) {
        if (printk_ratelimit())
            printk(KERN_NOTICE "snull rx: low on mem - packet dropped\n");
        priv->stats.rx_dropped++;
        goto out;
    }
    skb_reserve(skb, 2); /* align IP on 16B boundary */


    memcpy(skb_put(skb, pkt->datalen), pkt->data, pkt->datalen);

    /* Write metadata, and then pass to the receive level */
    skb->dev = dev;
    skb->protocol = vlc_type_trans(skb, dev);
    skb->ip_summed = CHECKSUM_UNNECESSARY; /* don't check it */

    skb_tmp = skb;
    skb_reset_network_header(skb_tmp);

    iph = (struct iphdr *) skb_network_header(skb_tmp);
    addr = (unsigned int) iph->saddr;
    Daddr = (unsigned int) iph->daddr;

    printk("Source IP: ");
    bytes[0] = addr & 0xFF;
    bytes[1] = (addr >> 8) & 0xFF;
    bytes[2] = (addr >> 16) & 0xFF;
    bytes[3] = (addr >> 24) & 0xFF;
    printk("%d.%d.%d.%d\n", bytes[0], bytes[1], bytes[2], bytes[3]);


    printk("Destination IP: ");
    bytes[0] = Daddr & 0xFF;
    bytes[1] = (Daddr >> 8) & 0xFF;
    bytes[2] = (Daddr >> 16) & 0xFF;
    bytes[3] = (Daddr >> 24) & 0xFF;
    printk("%d.%d.%d.%d\n", bytes[0], bytes[1], bytes[2], bytes[3]);

    pkt->destination = Daddr;
    pkt->source = addr;
build_routing_table
    //If I am not destination at network layer, add package to queue to forward.
    if(Daddr != self_ip){
        printk("Destination address is different. Enqueue package\n");
        
        dev->trans_start = jiffies;
        tmp_pkt = vlc_get_tx_buffer(dev);
        tmp_pkt->next = NULL; ///*******
        tmp_pkt->datalen = pkt->datalen;
        tmp_pkt->destination = Daddr;
        tmp_pkt->source = addr;

        memcpy(tmp_pkt->data, pkt->data, pkt->datalen);
        vlc_enqueue_pkt(dev, tmp_pkt);
        dev_kfree_skb(skb);
//        printk("Enqueue Package\n");
        goto out;
    }
    
    priv->stats.tx_packets++;
    priv->stats.tx_bytes += pkt->datalen;

    netif_rx(skb);
out:
    return;
}

static inline int decode_a_symbol_w_Manchester_RLL(void) {
    int val_1, val_2;
    val_1 = rx_buffer[rx_out_index++];
    if (rx_out_index >= RX_BUFFER_SIZE)
        rx_out_index = 0;
    val_2 = rx_buffer[rx_out_index++];
    if (rx_out_index >= RX_BUFFER_SIZE)
        rx_out_index = 0;
    
    return (val_1 < val_2);
}

static _Bool rx_buffer_has_two_plus_symbols(void) {
    if ((((rx_out_index + 1) % RX_BUFFER_SIZE) != rx_in_index)
            && (rx_out_index % RX_BUFFER_SIZE != rx_in_index))
        return true;
    return false;
}

//static void print_rx_symbols(void) {
//    int i = rx_out_index;
//    printk("Read values: ");
//    while (i != rx_in_index && i < rx_out_index + 8) { // Print 16 symbols
//        // while (i!=rx_in_index) { // Print 16 symbols
//        printk("%d ", (int) rx_buffer[i++]);
//        if (i >= RX_BUFFER_SIZE)
//            i = 0;
//    }
//    if (adc_ch == 0)
//        printk("- RX: low-power LED.\n");
//    else
//        printk("- RX: photodiode.\n");
//}

/// Can not use the standard "strncmp" or "strcmp"

static int cmp_packets(char *pkt_1, char *pkt_2, int len) {
    int i = 0;
    for (i = 0; i < len; i++) {
        //printk("%d || %d\n", pkt_1[i],pkt_2[i]);
        if (pkt_1[i] != pkt_2[i])
            return 1; // Packets are not equal
    }
    return 0; // Packets are equal
}





static int phy_decoding(void *data) {
    
    
    int jStartU, jEndU, timeCount;
    int diff = 0;
    int backoff_timer = 0;
    unsigned int dst_addr = 0;
    unsigned int unr_addr = 0;
    unsigned int src_addr = 0;
    int hop_count, hop_fill,max_hop;
    int c = 0;
    int ip_n = 0;
    int seq_n = 0;
    int sum = 0;
    int average = 0;
    int isDestination = 0;
    int isDestinationRREQ = 0;
    int i, j, num_err = 0;
    
    
    int destination_id_received = 0;
    int source_id_received = 0;
    
    char sfd_data = 0xa3;
    char sfd_ack = 0x3a;
    
    char sfd_reqId = 0x6a;
    char sfd_repId = 0x7a;
    
    
    char sfd_rreq = 0x9a;
    char sfd_rrep = 0xa9;
    char sfd_rerr = 0xa8;
    
    
    int cnt_preamble = 0;
    int cnt_byte = 0;
    int cnt_symbol = 1;
    int curr_symbol = 0;
    _Bool curr_read = 0;
    //_Bool curr_cmp;
    char recv_sfd = 0;
    unsigned int payload_len = 0;
    unsigned int buffer_len = 0;
    int rx_ch;
    int prev_symbol = 0;
    int guarder = 2; //
    int sum_preamble = 0;
    int min_preamble = 10;
    int max_preamble = 0;
    
    int co = 0;
    int ips[10];
    int ips_rrep[10];
    
    
    //unsigned short recv_crc = 0;
    //unsigned short cal_crc = 0;
    struct vlc_priv *priv = netdev_priv(vlc_devs);
    int index_block, encoded_len, num_of_blocks, total_num_err = 0;
    // May 31, 2015
    //int max_un_reception = 10000;
    ///******************Nov 14, 2014
    //int erasures[16];
    //int n_erasures = 0;
    ///******************************
    threshold = 0;

start_of_process: /// Start the process of a frame
    sum = 0;
    average = 0;
    cnt_symbol = 1;
    prev_symbol = 0;
    cnt_preamble = 0;
    sum_preamble = 0;
    tmp_symbol_index = 0;
    for (;;) {
        //I can't decode if I'm transmitting something.. wait for a decoding slot..
        if (phy_state == TX ) {
            rx_out_index = rx_in_index;
            msleep(decoding_sleep_slot);
            //printk("sleep...\n");
            continue;
        }
        while (rx_out_index != rx_in_index) {
            curr_symbol = rx_buffer[rx_out_index++];
            buffer_sensing[free_position_sensing++] = curr_symbol;

            if (rx_out_index >= RX_BUFFER_SIZE) {
                rx_out_index = 0;
            }
            if (((!(cnt_symbol % 2)) && (curr_symbol - prev_symbol >= guarder))
                    || ((cnt_symbol % 2)&&(prev_symbol - curr_symbol >= guarder))) {
                ++cnt_symbol;
                sum_preamble += curr_symbol;
                max_preamble = (curr_symbol > max_preamble) ? curr_symbol : max_preamble;
                min_preamble = (curr_symbol < min_preamble) ? curr_symbol : min_preamble;
            } else {
                memset(buffer_sensing, 0, sizeof (int) * MAX_SYMBOLS_SENSING);
                free_position_sensing = 0;
                cnt_symbol = 1;
                sum_preamble = curr_symbol;
                min_preamble = 100;
                max_preamble = 0;
                cnt_preamble = 0;
            }
            prev_symbol = curr_symbol;
            if (cnt_symbol >= 8) {
                cnt_symbol = 0;
                ++cnt_preamble;

                if (cnt_preamble >= PREAMBLE_LEN) {

                    threshold = (min_preamble + max_preamble) / 2;
                    if (show_msg)
                        printk("Found a preamble! ");
                    goto p_sfd;
                }
            }
        }
        msleep(decoding_sleep_slot);
        if (kthread_should_stop()) {
            printk("\n=======EXIT======from p_premable=======\n");
            goto ret;
        }
    }

p_sfd: /// Proceed the SFD
    cnt_symbol = 7;
    recv_sfd = 0;
    for (;;) {
        while (rx_buffer_has_two_plus_symbols()) {
            curr_read = decode_a_symbol_w_Manchester_RLL();
            recv_sfd += (curr_read << cnt_symbol);
            if (--cnt_symbol < 0) {
                if (recv_sfd == sfd_data && !f_wait_for_ack && !f_wait_for_rrep) {
//                    printk("Received Data\n");

                    goto p_length;
                } else if (recv_sfd == sfd_ack) {
                    
                    goto p_ack;   
                    
                    if (!kthread_should_stop())
                        goto start_of_process;               


                } else if (recv_sfd == sfd_reqId) { // REQ ID
//                    printk("Received REQ_ID.\n");
                    f_received_reqId = true;
                    while (buffer_sensing[co] != 0) {
                        sum += buffer_sensing[co++];
                        //printk("%d ", buffer_sensing[co-1]);
                    }
                    average = sum / (co - 1);
//                    co = 0;
//                    printk("Average sensing: %d\n", average);
                    goto p_arp;
                } else if (recv_sfd == sfd_repId) { // REP ID
//                    printk("Received REPID.\n");
                    f_received_repId = true;
                    while (buffer_sensing[co] != 0) {
                        sum += buffer_sensing[co++];
                    }
                    average = sum / (co - 1);
                    co = 0;
                    //printk("Average sensing: %d\n", average);
                    
                    goto p_arp;

                } else if (recv_sfd == sfd_rreq && !f_wait_for_ack && !f_wait_for_rrep) {
                    //If I listen to help, go to p_length to forward frame
                    printk("Received RREQ.. process it....\n");
                    
                    goto p_length;
                } else if (recv_sfd == sfd_rrep && !f_wait_for_ack) {
                    //If I listen to help, go to p_length to forward frame
                    printk("Received RREP.. process it....\n");
                    
                    goto p_length;
                } else if (recv_sfd == sfd_rerr && !f_wait_for_ack && !f_wait_for_rrep) {
                    //If I listen to help, go to p_length to forward frame
                    printk("Received RERR.. process it....\n");
                    
                    goto p_length;
                } else {
                    //printk("SFD error: %02x\n", recv_sfd&0xff);
                    goto start_of_process;
                }
            }
        }
        msleep(decoding_sleep_slot);
        if (kthread_should_stop()) {
            printk("\n=======EXIT======from p_sfd=======\n");
            goto ret;
        }
    }



p_ack: // Check if ACK is for me
    cnt_symbol = 15;
    rx_ch = 0;
    c = 0;
    
    for (;;) {
        while (rx_buffer_has_two_plus_symbols()) {
            curr_read = decode_a_symbol_w_Manchester_RLL();
            rx_ch += (curr_read << cnt_symbol);
            if (--cnt_symbol < 0) {
                cnt_symbol = 15;
                destination_id_received = rx_ch; 
                if(destination_id_received == self_id){
                    printk("Received ACK - It is for me.\n");
                    f_wait_for_ack = false;
                    f_received_ack = true;
                    goto end;
                }else{
                    printk("Received ACK - It is NOT for me.\n");   
                    goto end;
                }                               
                
            }   
        }
        msleep(decoding_sleep_slot);
        if (kthread_should_stop()) {
            printk("\n=======EXIT======from p_length=======\n");
            //if (rx_data) kfree(rx_data);
            goto ret;
        }
    }                    
    
p_arp:
    cnt_byte = 0;
    rx_ch = 0;
    c = 1;                    
    cnt_symbol = 15;
    
    // The REQ_ID and REP_ID both have three fields: Destination id, Source id and Source IP.
    // We go field by field, taking into consideration that both ID fields have 2 bytes, while the IP has 4.
    for (;;) {
        while (rx_buffer_has_two_plus_symbols()) {
            curr_read = decode_a_symbol_w_Manchester_RLL();
            rx_ch += (curr_read << cnt_symbol);
            if (--cnt_symbol < 0) {
                if(c==1){
                    cnt_symbol = 15;
                    destination_id_received = rx_ch;              
                } else if (c==2){
                    source_id_received = rx_ch;
                    cnt_symbol = 31;
                } else if (c==3){
                    src_addr = rx_ch;
                    // If destination id is broadcast (0xff), send an REP_ID back
                    if(destination_id_received == broadcast_id){
                        //Answer the request with an REPID
                        printk("Req_id -- ");                    
                        printk("Destination id: %d  |  Source id: %d \n", destination_id_received, source_id_received);

                        addNodeToArpTable(source_id_received, src_addr, average);   
                        addNodeToRoutingTable(src_addr,src_addr,1,1, MAX_HOP);
                                                                        
                        generate_RepId_frame(source_id_received);

                        get_random_bytes(&backoff_timer, sizeof (backoff_timer));
                        CW = CW_min;
                        backoff_timer = abs(backoff_timer) % CW + 1;

        wait_repId: // waiting for ACK || receiving || transmitting || busy channel
                        while ((phy_state == TX) || basic_sensing(false)) {
                            //if our phy_state is TX, it means we are transmitting the ACK frame..
                            //Basic sensing is the method we call in order to 
                            //sense the channel and check if we are able to transmit... if it
                            //returns true, it means the channel is busy 
                            //printk("Basic sensing...\n");
                            msleep(decoding_sleep_slot);
                            if (kthread_should_stop()) {
                                printk("\n=======EXIT mac_tx=======\n");
                                return 0;
                            }
                        }
                        backoff_timer -= 1;
                        //backoff timer... goes down until we are sure the channel is free..
                        while (backoff_timer)
                            goto wait_repId;
                        //From this point on, we are actually transmitting our data..

                        tx_repId_curr_index = 0;
                        f_ready_to_tx_repId = 1;

                        if (!hpl) switch_led_to_tx();
                        phy_state = TX; // Switch to TX mode to transmit the data frame

                        dst_addr = 0;
                        goto end;                        
                    } else if(destination_id_received == self_id){
                        //Receiving REPID
                        printk("Rep_id -- ");
                        printk("Destination id: %d  |  Source id: %d \n", destination_id_received, source_id_received);

                        addNodeToArpTable(source_id_received, src_addr, average);        
                        addNodeToRoutingTable(src_addr,src_addr,1,1,MAX_HOP);
                    }
                    
                    goto end;
                }
                
                rx_ch = 0;
                c++;               
                


            }
        }
        
        msleep(decoding_sleep_slot);
        if (kthread_should_stop()) {
            printk("\n=======EXIT======from p_body=======\n");
            //if (rx_data) kfree(rx_data);
            goto ret;
        }
    }


p_length: ///  Get payload length
    cnt_symbol = 15;
    payload_len = 0;
    for (;;) {
        while (rx_buffer_has_two_plus_symbols()) {
            curr_read = decode_a_symbol_w_Manchester_RLL();
            payload_len += (curr_read << cnt_symbol);
            if (--cnt_symbol < 0) {
                if (payload_len > 0 && payload_len <= mtu)
                    printk("Payload: %d \n", payload_len);
                else
                    goto end;
                encoded_len = payload_len + 2 * MAC_ADDR_LEN + PROTOCOL_LEN;
                // Calculate the number of blocks
                if (encoded_len % block_size)
                    num_of_blocks = encoded_len / block_size + 1;
                else
                    num_of_blocks = encoded_len / block_size;
                //printk("num_of_blocks: %d\n", num_of_blocks);
                buffer_len = MAC_HDR_LEN + payload_len + num_of_blocks*ECC_LEN;
                //rx_data = kmalloc(buffer_len, GFP_KERNEL);
                memset(rx_data, 0, sizeof (char) * RX_DATA_LEN);
                rx_data[0] = (char) ((payload_len >> 8) & 0xff);
                rx_data[1] = (char) (payload_len & 0xff);
                tmp_symbol_index = 0;
                goto p_body;
            }
        }
        msleep(decoding_sleep_slot);
        if (kthread_should_stop()) {
            printk("\n=======EXIT======from p_length=======\n");
            //if (rx_data) kfree(rx_data);
            goto ret;
        }
    }

p_body: /// Proceed the frame payload
    cnt_byte = 2; // The two bytes are the data length, which has been saved
    cnt_symbol = 7;
    rx_ch = 0;
    for (;;) {
        while (rx_buffer_has_two_plus_symbols()) {
            curr_read = decode_a_symbol_w_Manchester_RLL();
            rx_ch += (curr_read << cnt_symbol);
            if (--cnt_symbol < 0) {
                rx_data[cnt_byte++] = (char) rx_ch;
                if (cnt_byte >= buffer_len) {
                    goto p_final;
                }
                cnt_symbol = 7;
                rx_ch = 0;
            }
        }
        msleep(decoding_sleep_slot);
        if (kthread_should_stop()) {
            printk("\n=======EXIT======from p_body=======\n");
            //if (rx_data) kfree(rx_data);
            goto ret;
        }
    }



p_rreq:

    //Check the length of the IP addresses.
    //Processing rreq:
    //Source addr: 4 bytes
    //Dest addr: 4 bytes
    //IP length: 4 byte
    //Ips... n bytes.
                    
    //Remember: The routing message is the payload of the frame.
    //Therefore, it starts at byte 6 of the buffer. 0|1 - Payload Length . 2|3 - Dest MAC . 4|5 - Src MAC. 
                    
//    printk("Start decoding RREQ...\n");    

    max_hop = (rx_data[6] << 24) + (rx_data[7] << 16) + (rx_data[8] << 8) + rx_data[9];
//    printk("Max hop: %d\n", max_hop);

    hop_count = (rx_data[10] << 24) + (rx_data[11] << 16) + (rx_data[12] << 8) + rx_data[13];
//    printk("Hop count: %d\n", hop_count);
    hop_fill = hop_count;
    
    if(hop_count >= max_hop){
        goto end;
    }
        
    src_addr = (rx_data[14] << 24) + (rx_data[15] << 16) + (rx_data[16] << 8) + rx_data[17];
    if(src_addr == self_ip){
//        printk("Receiving same rreq sent earlier.... ignore it\n");
        goto end;
    }
    
    printk("Received RREQ from node: ");
//    print_ip(src_addr); printk("\n");


    dst_addr = (rx_data[18] << 24) + (rx_data[19] << 16) + (rx_data[20] << 8) + rx_data[21];
//    printk("Destination: ");
//    print_ip(dst_addr); printk("\n");

    //Check if I am the destination node. If so, sabe it for the last part..
    if(dst_addr == self_ip){
        isDestinationRREQ = 1;
        forwardRreq = 0;
    } else {
        isDestinationRREQ = 0;
        forwardRreq = 1;
    }

    
    seq_n = (rx_data[22] << 24) + (rx_data[23] << 16) + (rx_data[24] << 8) + rx_data[25];
    printk("Sequence number: %d \n", seq_n);
    if(!routeIsFresh(dst_addr, seq_n)){
//        printk("Another route is fresher than this one. Ignore RREQ... \n");
        goto end;
    }
    
    ip_n = (rx_data[26] << 24) + (rx_data[27] << 16) + (rx_data[28] << 8) + rx_data[29];
//    printk("Number of ips: %d -- ", ip_n);
    
    
    // More IPs encapsulated.. go through them, adding one by one to an array
    if(ip_n>0){
        c = 0;
        while(c<ip_n){
            int ip = (rx_data[30+c*4] << 24) + (rx_data[31+c*4] << 16) + (rx_data[32+c*4] << 8) + rx_data[33+c*4];
            if(ip == self_ip){
//                printk("This RREQ is travelling and I'm already in the trail... Ignore it...\n");
                goto end;
            }
            print_ip(ip); printk(",");
            ips[c] = ip;  
            c++;
        }
        printk("\n");
        
        //Now that I have the source address, and all the trail the RREQ formed along DA WAE
//        printk("Finished passing through array...\n");
        addNodeToRoutingTable(src_addr, ips[ip_n-1], seq_n, hop_fill, max_hop);
        hop_fill--;
        for(i = 0; i< ip_n; i++){
            addNodeToRoutingTable(ips[i], ips[ip_n-1], seq_n, hop_fill, max_hop);
            hop_fill--;
        }

        ips[ip_n] = self_ip;

        if(isDestinationRREQ!=1){
            generate_Rreq_frame_wTrail(src_addr, dst_addr, ip_n+1, ips, seq_n, hop_count, max_hop);
        }else{
            ips_rrep[0] = get_next_hop_from_routingTable(src_addr);
            generate_Rrep_frame_wTrail(self_ip, src_addr , 1, ips_rrep, seq_n, 0, max_hop);
//            printk("Prepare rrep.... \n");                        
        }

        get_random_bytes(&backoff_timer, sizeof (backoff_timer));
        CW = CW_min;
        backoff_timer = abs(backoff_timer) % CW + 1;                    

        wait1_rreq: 
        while ((phy_state == TX) || basic_sensing(false)) {
            msleep(decoding_sleep_slot);
            if (kthread_should_stop()) {
                printk("\n=======EXIT mac_tx=======\n");
                return 0;
            }
        }
        backoff_timer -= 1;
        //backoff timer... goes down until we are sure the channel is free..
        while (backoff_timer)
            goto wait1_rreq;
        //From this point on, we are actually transmitting our data..

        if(isDestinationRREQ!=1){
            tx_rreq_curr_index = 0;
            f_ready_to_tx_rreq = 1;                        
        } else {
            tx_rrep_curr_index = 0;
            f_ready_to_tx_rrep = 1;                            
        }

        if (!hpl) switch_led_to_tx();
        phy_state = TX; // Switch to TX mode to transmit the data frame

        msleep(decoding_sleep_slot);
        goto end;               


        if (kthread_should_stop()) {
            printk("\n=======EXIT======from p_body=======\n");
            //if (rx_data) kfree(rx_data);
            goto ret;
        }

    } else {
        printk("\n");
        //This is the first node the rreq is passing through
        // This is the first node and not destiantion
        if(forwardRreq){
            addNodeToRoutingTable(src_addr, src_addr, seq_n, hop_count, max_hop);
            ips[0] = self_ip;
            generate_Rreq_frame_wTrail(src_addr, dst_addr, ip_n+1, ips, seq_n, hop_count, max_hop);

            get_random_bytes(&backoff_timer, sizeof (backoff_timer));
            CW = CW_min;
            backoff_timer = abs(backoff_timer) % CW + 1;

            wait_rreq: 
            while ((phy_state == TX) || basic_sensing(false)) {
                msleep(decoding_sleep_slot);
                if (kthread_should_stop()) {
                    printk("\n=======EXIT mac_tx=======\n");
                    return 0;
                }
            }
            backoff_timer -= 1;
            while (backoff_timer)
                goto wait_rreq;

            tx_rreq_curr_index = 0;
            f_ready_to_tx_rreq = 1;

            if (!hpl) switch_led_to_tx();
            phy_state = TX; // Switch to TX mode to transmit the data frame

            msleep(decoding_sleep_slot);
            goto end;                           
        } else {
            // One hop and reached destination. Send rrep back to source 
            addNodeToRoutingTable(src_addr, src_addr, seq_n, hop_count, max_hop);
            ips_rrep[0] = get_next_hop_from_routingTable(src_addr);
            generate_Rrep_frame_wTrail(self_ip, src_addr, 1, ips_rrep, seq_n, 0, max_hop);
            msleep(50*decoding_sleep_slot);
//            printk("Prepare rrep.... \n");                            

            get_random_bytes(&backoff_timer, sizeof (backoff_timer));
            CW = CW_min;
            backoff_timer = abs(backoff_timer) % CW + 1;                    

            wait3_rrep: 
            while ((phy_state == TX) || basic_sensing(false)) {
                msleep(decoding_sleep_slot);
                if (kthread_should_stop()) {
                    printk("\n=======EXIT mac_tx=======\n");
                    return 0;
                }
            }
            backoff_timer -= 1;
            while (backoff_timer)
                goto wait3_rrep;

            tx_rrep_curr_index = 0;
            f_ready_to_tx_rrep = 1;                              

            if (!hpl) switch_led_to_tx();
            phy_state = TX; // Switch to TX mode to transmit the data frame

            msleep(decoding_sleep_slot);
            goto end; 
        }
    }
    
    


p_rrep:    
                    
    //Remember: The routing message is the payload of the frame.
    //Therefore, it starts at byte 6 of the buffer. 0|1 - Payload Length . 2|3 - Dest MAC . 4|5 - Src MAC. 
//    printk("Start decoding RREP...\n"); 

    max_hop = (rx_data[6] << 24) + (rx_data[7] << 16) + (rx_data[8] << 8) + rx_data[9];
//    printk("Max hop: %d \n", max_hop);

    hop_count = (rx_data[10] << 24) + (rx_data[11] << 16) + (rx_data[12] << 8) + rx_data[13];
//    printk("Hop count: %d \n", hop_count);
    hop_fill = hop_count;

    src_addr = (rx_data[14] << 24) + (rx_data[15] << 16) + (rx_data[16] << 8) + rx_data[17];
    if(src_addr == self_ip){
//        printk("Receiving same rrep sent earlier.... ignore it\n");
        goto end;
    }
    
//    printk("Received RREP from node: ");
//    print_ip(src_addr); printk("\n");


    dst_addr = (rx_data[18] << 24) + (rx_data[19] << 16) + (rx_data[20] << 8) + rx_data[21];
//    printk("Destination: ");
//    print_ip(dst_addr); printk("\n");

    isDestination = 0;
    //Check if I am the destination node. If so, sabe it for the last part..
    if(dst_addr == self_ip){
        isDestination = 1;
        f_received_rrep = 1;
//        printk("Arrived at destination...\n");
    } else {
        isDestination = 0;
    }

    seq_n = (rx_data[22] << 24) + (rx_data[23] << 16) + (rx_data[24] << 8) + rx_data[25];
//    printk("Sequence number: %d \n", seq_n);

    ip_n = (rx_data[26] << 24) + (rx_data[27] << 16) + (rx_data[28] << 8) + rx_data[29];
//    printk("Number of ips: %d  -- ", ip_n);
    
    //ip_n will always be more than 1, because it contains the next hop back to the source node who requested the route
    
    
    // More IPs encapsulated.. go through them, adding one by one to an array
    c = 0;
    while(c< ip_n){
        int ip = (rx_data[30+c*4] << 24) + (rx_data[31+c*4] << 16) + (rx_data[32+c*4] << 8) + rx_data[33+c*4];
        print_ip(ip); printk(", ");
        ips_rrep[c] = ip;  
        c++;
    }
    printk("\n");
    if(ips_rrep[c-1]==self_ip || isDestination){
        if(ip_n==1){
            //First hop of the RREP
            addNodeToRoutingTable(src_addr, src_addr, seq_n, hop_count, max_hop);            
        } else {
            //Now that I have the source address, and all the trail the RREQ formed along DA WAE
//            printk("Finished passing through array...\n");
            addNodeToRoutingTable(src_addr, ips_rrep[ip_n-2], seq_n, hop_fill, max_hop);
            hop_fill--;
            for(i = 0; i< ip_n-1; i++){
                addNodeToRoutingTable(ips_rrep[i], ips_rrep[ip_n-2], seq_n, hop_fill, max_hop);
                hop_fill--;
            }            
        }
        
        if(isDestination){
            goto end;
        }
        
        // If next hop is destination, no need to attach its IP address to it
        if(get_next_hop_from_routingTable(dst_addr) != dst_addr){        
        
            ips_rrep[ip_n] = get_next_hop_from_routingTable(dst_addr);
    //        printk("Prepare RREP to forward...\n");
            generate_Rrep_frame_wTrail(src_addr, dst_addr , ip_n+1, ips_rrep, seq_n, hop_count, max_hop);
            
        } else {
            
            generate_Rrep_frame_wTrail(src_addr, dst_addr , ip_n, ips_rrep, seq_n, hop_count, max_hop);
            
        }
        
        get_random_bytes(&backoff_timer, sizeof (backoff_timer));
        CW = CW_min;
        backoff_timer = abs(backoff_timer) % CW + 1;                    

        wait_rrep_forward: 
        while ((phy_state == TX) || basic_sensing(false)) {
            msleep(decoding_sleep_slot);
            if (kthread_should_stop()) {
                printk("\n=======EXIT mac_tx=======\n");
                return 0;
            }
        }
        backoff_timer -= 1;
        //backoff timer... goes down until we are sure the channel is free..
        while (backoff_timer)
            goto wait_rrep_forward;
        //From this point on, we are actually transmitting our data..

        tx_rrep_curr_index = 0;
        f_ready_to_tx_rrep = 1;                            
        

        if (!hpl) switch_led_to_tx();
        phy_state = TX; // Switch to TX mode to transmit the data frame

        msleep(decoding_sleep_slot);
        goto end; 
        
    } else {
//        printk("RREP is travelling back to source.. I'm not in the middle.\n");
        goto end;        
    }


 p_rerr:    
                    
//    printk("Start decoding RERR...\n");        
    jStart = jiffies;
    jStartU_global = jiffies_to_msecs(jStart);

    max_hop = (rx_data[6] << 24) + (rx_data[7] << 16) + (rx_data[8] << 8) + rx_data[9];
//    printk("Max hop: %d \n", max_hop);

    hop_count = (rx_data[10] << 24) + (rx_data[11] << 16) + (rx_data[12] << 8) + rx_data[13];
//    printk("Hop count: %d \n", hop_count);

    src_addr = (rx_data[14] << 24) + (rx_data[15] << 16) + (rx_data[16] << 8) + rx_data[17];   
//    printk("Source of route: ");print_ip(src_addr); printk("\n");

    unr_addr = (rx_data[18] << 24) + (rx_data[19] << 16) + (rx_data[20] << 8) + rx_data[21];   
//    printk("Route to node ");print_ip(unr_addr); printk(" is broken.\n");
    
    if(self_ip == unr_addr){        
        goto end;
    }
    
    //checkRerr - check if next hop of this node is the same as source node
    if(checkRerr(unr_addr, src_addr)){
    
        breakRouteToNode(unr_addr);
        generate_Rerr_frame(unr_addr);              
                
        get_random_bytes(&backoff_timer, sizeof (backoff_timer));
        CW = CW_min;
        backoff_timer = abs(backoff_timer) % CW + 1;

        wait_rerr_forward: // waiting for ACK || receiving || transmitting || busy channel
        while ((phy_state == TX) || basic_sensing(false)) {
            msleep(decoding_sleep_slot);
            if (kthread_should_stop()) {
                printk("\n=======EXIT mac_tx=======\n");
                return 0;
            }
        }
        backoff_timer -= 1;
        while (backoff_timer)
            goto wait_rerr_forward;                

        tx_rerr_curr_index = 0;
        f_ready_to_tx_rerr = 1;      
        if (!hpl) switch_led_to_tx();
        phy_state = TX; // Switch to TX mode to transmit the ACK frame                 
        msleep(10 * decoding_sleep_slot);
        goto end;         
    } else {
        goto end;
    }
                    
    
p_final: /// Check the Reed-solomon, print the DATA frame, and send the ACK

    // At this point, I should be able to get both source and destination address.
    cnt_byte = 2;
    dst_addr = rx_data[cnt_byte + 1] | rx_data[cnt_byte] << 8;
    src_addr = rx_data[cnt_byte + 3] | rx_data[cnt_byte + 2] << 8;

//    printk("Destination Id: %d || Source Id: %d || Self Id: %d \n", dst_addr, src_addr, self_id);
    if(self_id == dst_addr || dst_addr == broadcast_id){
//        printk("It's for me at MAC Layer\n");
    } else {
//        printk("Not for me.. ignore the frame\n");
        
        goto end;
    }
    
    j = OCTET_LEN + 2 * MAC_ADDR_LEN + PROTOCOL_LEN + payload_len;
    total_num_err = 0;
    //print_coded_frame();
    for (index_block = 0; index_block < num_of_blocks; index_block++) {
        for (i = 0; i < ECC_LEN; i++) {
            par[i] = rx_data[j + index_block * ECC_LEN + i];
            //printk(" %02x", par[i]);
        }
        //printk("\n");
        if (index_block < num_of_blocks - 1) {
            num_err = decode_rs8(rs_decoder, rx_data + OCTET_LEN + index_block*block_size,
                    par, block_size, NULL, 0, NULL, 0, NULL);
        } else { // The last block
            num_err = decode_rs8(rs_decoder, rx_data + OCTET_LEN + index_block*block_size,
                    par, encoded_len % block_size, NULL, 0, NULL, 0, NULL);
        }
        if (num_err < 0) {
            printk("*** CRC error. ***\n");
            f_adjust_slot = 1;
            goto end;
        }
        total_num_err += num_err;
    }
    if (total_num_err > 0) {
        printk("*** Has corrected %d error(s) *** \n", total_num_err);
    }

    if (payload_len > 0) { // Data frame
        if (mac_or_app == APP) { // To upper layer
            
            //rx_pkt = kmalloc (sizeof (struct vlc_packet), GFP_KERNEL);
            rx_pkt->datalen = MAC_HDR_LEN - OCTET_LEN + payload_len;
            // Copy the received data (omit the datalen octet)
            memcpy(rx_pkt->data, rx_data + OCTET_LEN, rx_pkt->datalen);
            
            if (cmp_packets(rx_pkt->data, rx_pkt_check_dup->data, rx_pkt->datalen)) {

                if(recv_sfd==sfd_rreq){
                    f_received_rreq = true;
                    goto p_rreq;
                }

                if(recv_sfd==sfd_rrep){
                    f_wait_for_rrep = false;
                    goto p_rrep;
                }

                if(recv_sfd==sfd_rerr){
                    f_received_rerr = true;
                    goto p_rerr;
                }
            
                // Frist copy the packet; the rx_pkt will be released in max_rx
                memcpy(rx_pkt_check_dup->data, rx_pkt->data, rx_pkt->datalen);
                rx_pkt_check_dup->datalen = rx_pkt->datalen;

                //Retransmit the received frame or send it to upper layers.
                //If it is help, it means I'm receiving the help signal from the transmitter.. it's duplicate

                generate_ACK_frame(src_addr);            
                msleep(10 * decoding_sleep_slot);           
                
                tx_ack_curr_index = 0;
                f_ready_to_tx_ack = 1;
                phy_state = TX; // Switch to TX mode to transmit the ACK frame       
                
                
                mac_rx(vlc_devs, rx_pkt);
                priv->stats.rx_packets++;
                priv->stats.rx_bytes += rx_pkt->datalen;
                
                //printk("Received DATA. %d \n", adc_ch);
                
            } else if (recv_sfd == sfd_data) {
                printk("Duplicate!.\n");
                //If it's duplicate, just send the ACK
                generate_ACK_frame(src_addr);               
                msleep(10 * decoding_sleep_slot);      
                
                tx_ack_curr_index = 0;
                f_ready_to_tx_ack = 1;
                phy_state = TX; // Switch to TX mode to transmit the ACK frame    
            }
        }
    }
end:
    //if (rx_data) kfree(rx_data);
    if (!kthread_should_stop())
        goto start_of_process;

ret:
    printk("\n=======EXIT phy_decoding=======\n");
    return 0;
}


//static enum hrtimer_restart phy_timer_handler(struct hrtimer *timer)

void phy_timer_handler(rtdm_timer_t *timer) {
    //int val1, val2 = 0;
    //int tmp_th = 10;
    //int prev_value = 0;
    if (f_adjust_slot) {
        rtdm_timer_stop(&phy_timer);
        rtdm_timer_start(&phy_timer, slot_ns / 2, slot_ns,
                RTDM_TIMERMODE_RELATIVE);
        f_adjust_slot = false;
    }

    hpl = (*(int *) tx_device->data); // Read the configuration of the TX
    switch_tx(); // May change the TX
    adc_ch = (*(int *) rx_device->data); // May change the RX
    
    countdownRoutingTable();    
    
    if (phy_state == TX) { // The node is in TX state
        if (f_ready_to_tx && (tx_data_curr_index < data_buffer_symbol_len)) {
            //printk("Ready to TX and still have things on buffer to send.\n");
            if (data_buffer_symbol[tx_data_curr_index]) {// Transmit symbol HIGH
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + CLEAR_OFFSET);
                delay_n_NOP();
                writel(1 << bit_led_anode, gpio2 + SET_OFFSET);
            } else { // Transmit symbol LOW
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);
                //gpio_set_value(GPIO_LED_ANODE, GPIOF_INIT_LOW);
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + SET_OFFSET);
            }
            if (++tx_data_curr_index >= data_buffer_symbol_len && f_ready_to_tx) {
                f_wait_for_ack = true;
                index_ack_timeout = 0;
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);
//                printk("Sent data.\n");
                gpio_set_value(GPIO_LED_ANODE, GPIOF_INIT_LOW);
                gpio_set_value(GPIO_BUFFER_CONTROL, GPIOF_INIT_HIGH);
                switch_led_to_rx();
                phy_state = RX; // Switch to RX mode to receive the ACK
                rx_out_index = rx_in_index;
            }
        }// End: the node is transmtting a data frame
        else if (f_ready_to_tx_ack && // Transmit an ACK frame
                (tx_ack_curr_index < ack_buffer_symbol_len)) {
            //printk("Ready to transmit ACK.\n");
            if (ack_buffer_symbol[tx_ack_curr_index]) {
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + CLEAR_OFFSET);
                delay_n_NOP();
                writel(1 << bit_led_anode, gpio2 + SET_OFFSET);
            } else {
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + SET_OFFSET);
            }
            if (++tx_ack_curr_index >= ack_buffer_symbol_len) {
                f_ready_to_tx_ack = false;
                printk("Sent ACK\n");
                if (!hpl) switch_led_to_rx();
                phy_state = RX; // Switch to RX mode
            }
        } else if (f_ready_to_tx_rreq && // Transmit a RREQ frame
                (tx_rreq_curr_index < rreq_buffer_symbol_len)) {
            if (rreq_buffer_symbol[tx_rreq_curr_index]) {
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + CLEAR_OFFSET);
                delay_n_NOP();
                writel(1 << bit_led_anode, gpio2 + SET_OFFSET);
            } else {
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + SET_OFFSET);
            }
            if (++tx_rreq_curr_index >= rreq_buffer_symbol_len) {
                index_rreq_timeout = 0;
                if(!forwardRreq)
                    f_wait_for_rrep = true;
                f_ready_to_tx_rreq = false;
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);

                printk("RREQ sent by node  ");print_ip(self_ip);printk("\n");

                gpio_set_value(GPIO_LED_ANODE, GPIOF_INIT_LOW);
                gpio_set_value(GPIO_BUFFER_CONTROL, GPIOF_INIT_HIGH);
                switch_led_to_rx();
                phy_state = RX; // Switch to RX mode  
                rx_out_index = rx_in_index;
            }
        } else if (f_ready_to_tx_rrep && // Transmit a RREQ frame
                (tx_rrep_curr_index < rrep_buffer_symbol_len)) {
            if (rrep_buffer_symbol[tx_rrep_curr_index]) {
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + CLEAR_OFFSET);
                delay_n_NOP();
                writel(1 << bit_led_anode, gpio2 + SET_OFFSET);
            } else {
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + SET_OFFSET);
            }
            if (++tx_rrep_curr_index >= rrep_buffer_symbol_len) {
                f_ready_to_tx_rrep = false;
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);

                printk("RREP sent by node  ");print_ip(self_ip);printk("\n");

                gpio_set_value(GPIO_LED_ANODE, GPIOF_INIT_LOW);
                gpio_set_value(GPIO_BUFFER_CONTROL, GPIOF_INIT_HIGH);
                switch_led_to_rx();
                phy_state = RX; // Switch to RX mode  
                rx_out_index = rx_in_index;
            }
        } else if (f_ready_to_tx_rerr && // Transmit a RREQ frame
                (tx_rerr_curr_index < rerr_buffer_symbol_len)) {
            if (rerr_buffer_symbol[tx_rerr_curr_index]) {
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + CLEAR_OFFSET);
                delay_n_NOP();
                writel(1 << bit_led_anode, gpio2 + SET_OFFSET);
            } else {
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + SET_OFFSET);
            }
            if (++tx_rerr_curr_index >= rerr_buffer_symbol_len) {
                f_ready_to_tx_rerr = false;
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);

                printk("RERR sent by node ");
                print_ip(self_ip);printk("\n");

                gpio_set_value(GPIO_LED_ANODE, GPIOF_INIT_LOW);
                gpio_set_value(GPIO_BUFFER_CONTROL, GPIOF_INIT_HIGH);
                switch_led_to_rx();
                phy_state = RX; // Switch to RX mode  
                rx_out_index = rx_in_index;
            }
        } else if (f_ready_to_tx_reqId && // Transmit a REQID frame
                (tx_reqId_curr_index < reqId_buffer_symbol_len)) {
            if (reqId_buffer_symbol[tx_reqId_curr_index]) {
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + CLEAR_OFFSET);
                delay_n_NOP();
                writel(1 << bit_led_anode, gpio2 + SET_OFFSET);
            } else {
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + SET_OFFSET);
            }
            if (++tx_reqId_curr_index >= reqId_buffer_symbol_len) {
                index_reqId_timeout = 0;
                f_ready_to_tx_reqId = false;
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);

                printk("REQ_ID sent by node ");
                print_ip(self_ip);printk("\n");

                gpio_set_value(GPIO_LED_ANODE, GPIOF_INIT_LOW);
                gpio_set_value(GPIO_BUFFER_CONTROL, GPIOF_INIT_HIGH);
                switch_led_to_rx();
                phy_state = RX; // Switch to RX mode  
                rx_out_index = rx_in_index;
            }
        } else if (f_ready_to_tx_repId &&
                (tx_repId_curr_index < repId_buffer_symbol_len)) {
            if (repId_buffer_symbol[tx_repId_curr_index]) {
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + CLEAR_OFFSET);
                delay_n_NOP();
                writel(1 << bit_led_anode, gpio2 + SET_OFFSET);
            } else {
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);
                if (!hpl) writel(1 << BIT_BUFFER_CONTROL, gpio2 + SET_OFFSET);
            }
            if (++tx_repId_curr_index >= repId_buffer_symbol_len) {
                index_repId_timeout = 0;
                f_ready_to_tx_repId = false;
                writel(1 << bit_led_anode, gpio2 + CLEAR_OFFSET);
                printk("Sent REP_ID.\n");
                gpio_set_value(GPIO_LED_ANODE, GPIOF_INIT_LOW);
                gpio_set_value(GPIO_BUFFER_CONTROL, GPIOF_INIT_HIGH);
                switch_led_to_rx();
                phy_state = RX; // Switch to RX mode     
                rx_out_index = rx_in_index;
            }
        }
    }// End TX mode
    else if (phy_state == RX) {
        rx_buffer[rx_in_index++] = SPI_read_from_adc();
        //printk("Value sensed: %d \n" , rx_buffer[rx_in_index-1]);
        if (rx_in_index >= RX_BUFFER_SIZE) {
            rx_in_index = 0;
        }
        if (f_wait_for_ack && (index_ack_timeout++) > ACK_TIMEOUT) {
            f_wait_for_ack = false;
            if (cnt_retransmission < MAX_RETRANSMISSION) {
                ++cnt_retransmission;
                f_re_tx = true; // Retransmit
//                printk("ACK timeout. Retransmit %d.\n", cnt_retransmission);
            } else {
                f_wait_for_ack = false;
//                printk("*** Drop frame: transmitted %d times ***\n", cnt_retransmission);
                f_dropped_frame = true;
                cnt_retransmission = 0;
                f_re_tx = false;
            }
        } else if (f_wait_for_rrep && (index_rreq_timeout++) > RREQ_TIMEOUT) {
            printk("RREQ Timeout finished\n");
            f_wait_for_rrep = false;
            f_rreq_timeout = true;
        }
    }// End RX mode
    else if (phy_state == SENSING) { /// The node is in SENSING state
        rx_in_val = SPI_read_from_adc();
        sensing_buffer[index_long_sensing++] = rx_in_val;
        rx_buffer[rx_in_index++] = rx_in_val; // Also save to the rx_buffer
        if (rx_in_index >= RX_BUFFER_SIZE) {
            rx_in_index = 0;
        }
        if (index_long_sensing >= LONG_SENSING_SYMBOLS) {
            phy_state = RX;
        }
    } // End Sensing mode (part of RX mode)
}

int vlc_release(struct net_device *dev) {
    //netif_stop_queue(dev); /* can't transmit any more */
    return 0;
}

// Configuration changes (passed on by ifconfig)

int vlc_config(struct net_device *dev, struct ifmap *map) {
    if (dev->flags & IFF_UP) /* can't act on a running interface */
        return -EBUSY;

    /* Don't allow changing the I/O address */
    if (map->base_addr != dev->base_addr) {
        printk(KERN_WARNING "snull: Can't change I/O address\n");
        return -EOPNOTSUPP;
    }

    /* Allow changing the IRQ */
    if (map->irq != dev->irq) {
        dev->irq = map->irq; /* request_irq() is delayed to open-time */
    }

    /* ignore other fields */
    return 0;
}

// Transmit a packet (called by the kernel)

int vlc_tx(struct sk_buff *skb, struct net_device *dev) {
    struct vlc_packet *tmp_pkt;
    struct iphdr *iph;
    unsigned int addr, Daddr;
    
    
    iph = (struct iphdr *) skb_network_header(skb);
    addr = (unsigned int) iph->saddr;

//    printk("Source IP: ");
//    bytes[0] = addr & 0xFF;
//    bytes[1] = (addr >> 8) & 0xFF;
//    bytes[2] = (addr >> 16) & 0xFF;
//    bytes[3] = (addr >> 24) & 0xFF;
//    printk("%d.%d.%d.%d\n", bytes[0], bytes[1], bytes[2], bytes[3]);

    Daddr = (unsigned int) iph->daddr;

//    printk("Destination IP: ");
//    bytes[0] = Daddr & 0xFF;
//    bytes[1] = (Daddr >> 8) & 0xFF;
//    bytes[2] = (Daddr >> 16) & 0xFF;
//    bytes[3] = (Daddr >> 24) & 0xFF;
//    printk("%d.%d.%d.%d\n", bytes[0], bytes[1], bytes[2], bytes[3]);


    dev->trans_start = jiffies;
    tmp_pkt = vlc_get_tx_buffer(dev);
    tmp_pkt->next = NULL; ///*******
    tmp_pkt->datalen = skb->len;
    tmp_pkt->destination = Daddr;
    tmp_pkt->source = addr;
    
    memcpy(tmp_pkt->data, skb->data, skb->len);
    vlc_enqueue_pkt(dev, tmp_pkt);
//    printk("Enqueue a packet!\n");

    dev_kfree_skb(skb);
    return 0; /* Our simple device can not fail */
}

// Ioctl commands

int vlc_ioctl(struct net_device *dev, struct ifreq *rq, int cmd) {
    //printk("ioctl\n");
    return 0;
}

// Return statistics to the caller

struct net_device_stats *vlc_stats(struct net_device *dev) {
    struct vlc_priv *priv = netdev_priv(dev);
    return &priv->stats;
}

// This function is called to fill up a VLC header 

int vlc_rebuild_header(struct sk_buff *skb) {
    struct vlchdr *vlc = (struct vlchdr *) skb->data;
    struct net_device *dev = skb->dev;

    memcpy(vlc->h_source, dev->dev_addr, dev->addr_len);
    memcpy(vlc->h_dest, dev->dev_addr, dev->addr_len);
    vlc->h_dest[MAC_ADDR_LEN - 1] ^= 0x01; /* dest is us xor 1 */
    return 0;
}

int vlc_header(struct sk_buff *skb, struct net_device *dev, unsigned short type,
        const void *daddr, const void *saddr, unsigned len) {
    struct vlchdr *vlc = (struct vlchdr *) skb_push(skb, VLC_HLEN);

    vlc->h_proto = htons(type);
    memcpy(vlc->h_source, saddr ? saddr : dev->dev_addr, dev->addr_len);
    memcpy(vlc->h_dest, daddr ? daddr : dev->dev_addr, dev->addr_len);
    vlc->h_dest[MAC_ADDR_LEN - 1] ^= 0x01; /* dest is us xor 1 */
    return (dev->hard_header_len);
}


static const struct header_ops vlc_header_ops = {
    .create = vlc_header,
    .rebuild = vlc_rebuild_header
};

int vlc_open(struct net_device *dev) {
    memcpy(dev->dev_addr, "\0\2", MAC_ADDR_LEN);
    netif_start_queue(dev);
    return 0;
}

// Setup VLC network device

void vlc_setup(struct net_device *dev) {
    dev->hard_header_len = VLC_HLEN;
    dev->mtu = mtu;
    dev->tx_queue_len = 100;
    dev->priv_flags |= IFF_TX_SKB_SHARING;
}

static const struct net_device_ops vlc_netdev_ops = {
    .ndo_open = vlc_open,
    .ndo_stop = vlc_release,
    .ndo_start_xmit = vlc_tx,
    .ndo_do_ioctl = vlc_ioctl,
    .ndo_set_config = vlc_config,
    .ndo_get_stats = vlc_stats,
};

int proc_read(char *page, char **start, off_t off, int count, int *eof,
        void *data) {
    count = sprintf(page, "%d", *(int *) data);
    return count;
}

int proc_write(struct file *file, const char *buffer,
        unsigned long count, void *data) {
    unsigned int c = 0, len = 0, val, sum = 0;
    int * temp = (int *) data;

    while (count) {
        if (get_user(c, buffer))
            return -EFAULT;

        len++;
        buffer++;
        count--;

        if (c == 10 || c == 0)
            break;
        val = c - '0';
        if (val > 9)
            return -EINVAL;
        sum *= 10;
        sum += val;
    }
    *temp = sum;
    return len;
}

void vlc_init(struct net_device *dev) {
    struct vlc_priv *priv;

    dev->addr_len = MAC_ADDR_LEN;
    dev->type = ARPHRD_ETHER; // ARPHRD_IEEE802154
    vlc_setup(dev); /* assign some of the fields */
    dev->netdev_ops = &vlc_netdev_ops;
    dev->header_ops = &vlc_header_ops;
    /* keep the default flags, just add NOARP */
    dev->flags |= IFF_NOARP;
    dev->features |= NETIF_F_HW_CSUM;

    priv = netdev_priv(dev);
    memset(priv, 0, sizeof (struct vlc_priv));
    //printk(".....4.....\n");
    //spin_lock_init(&priv->lock);
    //printk(".....5.....\n");
    if (mac_or_app == APP) {
        vlc_rx_ints(dev, 1); /* enable receive interrupts */
        tx_pkt = kmalloc(sizeof (struct vlc_packet), GFP_KERNEL);
        rx_pkt = kmalloc(sizeof (struct vlc_packet), GFP_KERNEL);
        rx_pkt_check_dup = kmalloc(sizeof (struct vlc_packet), GFP_KERNEL);
        tmp_pkt = kmalloc(sizeof (struct vlc_packet), GFP_KERNEL);

        if (tx_pkt == NULL || rx_pkt_check_dup == NULL || rx_pkt == NULL) {
            printk(KERN_NOTICE "Ran out of memory allocating packet pool\n");
            return;
        }
        rx_pkt_check_dup->datalen = 0;
        vlc_setup_pool(dev);
        priv->tx_queue = NULL;
        flag_exit = 0;
        //printk(".....8.....\n");
        //netif_start_queue(dev);
        //printk(".....9.....\n");
    }
}

void vlc_cleanup(void) {
    struct vlc_packet *pkt;
    struct vlc_priv *priv = netdev_priv(vlc_devs);
    ////unsigned long flags;
    //if (flag_lock)
    //spin_lock_bh(&priv->lock);
    flag_exit = 1;
    netif_stop_queue(vlc_devs);
    //if (flag_lock)
    //spin_unlock_bh(&priv->lock);


    // Clean the threads
    printk("stop phy decoding\n");
    if (task_phy_decoding) {
        kthread_stop(task_phy_decoding);
        task_phy_decoding = NULL;
    }

    printk("stop mac tx\n");
    if (task_mac_tx) {
        kthread_stop(task_mac_tx);
        task_mac_tx = NULL;
    }

    rtdm_timer_destroy(&phy_timer);

    iounmap(gpio1);
    iounmap(gpio2);

    // Clean the GPIOs
    gpio_free(GPIO_LED_ANODE);
    gpio_free(GPIO_LED_CATHODE);
    gpio_free(GPIO_BUFFER_CONTROL);
    gpio_free(GPIO_H_POWER_LED);
    gpio_free(GPIO_LED_OR_PD);

    gpio_free(SPI_CLC);
    gpio_free(SPI_MISO);
    gpio_free(SPI_MOSI);
    gpio_free(SPI_CS);
    // 

    // Clean the devices
    if (vlc_devs) {
        if (mac_or_app == APP) {
            printk("clean the pool\n");
            //if (flag_lock)
            //spin_lock_bh(&priv->lock);
            while (priv->tx_queue) {
                pkt = vlc_dequeue_pkt(vlc_devs);
                vlc_release_buffer(pkt);
            }
            //if (flag_lock)
            //spin_unlock_bh(&priv->lock);
            vlc_teardown_pool(vlc_devs);
            kfree(rx_pkt);
            kfree(rx_pkt_check_dup);
            kfree(tx_pkt);
        }
        printk("unregister the devs\n");
        unregister_netdev(vlc_devs);

        //if (mac_or_app == APP) {
        //vlc_teardown_pool(vlc_devs);
        //}

        printk("free the devs\n");
        free_netdev(vlc_devs);
    }

    remove_proc_entry("rx", vlc_dir);
    remove_proc_entry("tx", vlc_dir);
    remove_proc_entry("vlc", NULL);

    // Free the reed solomon resources
    if (rs_decoder) {
        free_rs(rs_decoder);
    }

    //printk("free packets\n");
    //if (tx_pkt)
    //kfree(tx_pkt);
    //if (rx_pkt)
    //kfree(rx_pkt);
    //if (rx_pkt_check_dup)
    //kfree(rx_pkt_check_dup);
    //if (data_buffer_symbol)
    //kfree(data_buffer_symbol);

    printk(KERN_NOTICE "The VLC module has been removed.\n");
    return;
}

static void vlc_regular_interrupt(int irq, void *dev_id, struct pt_regs *regs) {
}

int vlc_init_module(void) {
    int ret = -ENOMEM;
    printk("Initializing the VLC module...\n");

    vlc_interrupt = vlc_regular_interrupt;


    frq *= 1000; // Convert the frequency from KHz to Hz
    // May optimize this part
    /// Wait to be optimized
    decoding_sleep_slot = (1000 * 1 / frq);
    decoding_sleep_slot = (decoding_sleep_slot >= 1) ? decoding_sleep_slot : 1;
    printk("Sleep slot (while decoding) is %d ms\n", decoding_sleep_slot);

    /// Create the device and register it
    vlc_devs = alloc_netdev(sizeof (struct vlc_priv), "vlc%d", vlc_init);
    if (vlc_devs == NULL)
        goto out;
    ret = register_netdev(vlc_devs);
    if (ret)
        printk("VLC: error registering device \"%s\"\n", vlc_devs->name);



    /// GPIOs for the LED
    if (gpio_request(GPIO_LED_ANODE, "LED_ANODE")
            || gpio_request(GPIO_LED_CATHODE, "LED_CATHODE")
            || gpio_request(GPIO_BUFFER_CONTROL, "BUFFER_CONTROL")
            || gpio_request(GPIO_H_POWER_LED, "H_POWER_LED")
            || gpio_request(GPIO_LED_OR_PD, "LED_OR_PD")
            ) {
        printk("Request GPIO failed!\n");
        ret = -ENOMEM;
        goto out;
    }
    gpio_direction_output(GPIO_LED_ANODE, GPIOF_INIT_LOW);
    gpio_direction_output(GPIO_LED_CATHODE, GPIOF_INIT_LOW);
    gpio_direction_output(GPIO_BUFFER_CONTROL, GPIOF_INIT_HIGH);
    gpio_direction_output(GPIO_H_POWER_LED, GPIOF_INIT_LOW);
    gpio_direction_output(GPIO_LED_OR_PD, GPIOF_INIT_LOW);

    /// GPIOs for SPI
    if (gpio_request(SPI_CLC, "SPI_CLC")
            || gpio_request(SPI_MISO, "SPI_MISO")
            || gpio_request(SPI_MOSI, "SPI_MOSI")
            || gpio_request(SPI_CS, "SPI_CS")) {
        printk("Request GPIO failed!\n");
        ret = -ENOMEM;
        goto out;
    }
    gpio_direction_output(SPI_CLC, GPIOF_INIT_LOW);
    gpio_direction_input(SPI_MISO);
    gpio_direction_output(SPI_MOSI, GPIOF_INIT_LOW);
    gpio_direction_output(SPI_CS, GPIOF_INIT_LOW);

    // Qing - May 2, 2015
    //if (pd_as_rx == 1) { // PD
    //gpio_direction_output(GPIO_LED_OR_PD, GPIOF_INIT_HIGH);
    //} else { // LED
    //gpio_direction_output(GPIO_LED_OR_PD, GPIOF_INIT_LOW);
    //}
    //if (hpl == 1) {
    //bit_led_anode = BIT_H_POWER_LED; // High-power LED as TX
    //gpio_direction_output(GPIO_LED_OR_PD, GPIOF_INIT_LOW); // PD as RX
    //} else { // LED
    //bit_led_anode = BIT_LED_ANODE; // LED as TX
    //gpio_direction_output(GPIO_LED_OR_PD, GPIOF_INIT_HIGH); // LED as RX
    //}
    switch_tx();



    gpio1 = ioremap(ADDR_BASE_0, 4);
    gpio2 = ioremap(ADDR_BASE_1, 4);

    phy_state = RX;
    switch_led_to_rx();

    printk("my_gpio: Access address to device is:0x%x  0x%x\n",
            (unsigned int) gpio1, (unsigned int) gpio2);

    if (!(gpio1 && gpio2))
        goto out;

    vlc_dir = proc_mkdir("vlc", NULL);
    rx_device = create_proc_entry("rx", 0666, vlc_dir);
    tx_device = create_proc_entry("tx", 0666, vlc_dir);
    if (rx_device && tx_device) {
        rx_device->data = &rx_device_value;
        rx_device->read_proc = proc_read;
        rx_device->write_proc = proc_write;

        tx_device->data = &tx_device_value;
        tx_device->read_proc = proc_read;
        tx_device->write_proc = proc_write;
    }

    /// Timer
    slot_ns = 1000000000 / frq;
    printk("Slot in nanosecond: %d\n", slot_ns);
    ret = rtdm_timer_init(&phy_timer, phy_timer_handler, "phy timer");

    if (ret) {
        rtdm_printk("PWM: error initializing up-timer: %i\n", ret);
        return ret;
    }

    /* We could create the decoder on demand, if memory is a concern.
     * This way we have it handy, if an error happens
     *
     * Symbolsize is 10 (bits)
     * Primitve polynomial is x^10+x^3+1
     * first consecutive root is 0
     * primitve element to generate roots = 1
     * generator polinomial degree = 6
     */
    rs_decoder = init_rs(ecc_symsize, ecc_poly, 0, 1, ECC_LEN); // 0---FCR
    if (!rs_decoder) {
        printk(KERN_ERR "Could not create a RS decoder\n");
        ret = -ENOMEM;
        goto out;
    }


    ret = rtdm_timer_start(&phy_timer, slot_ns, slot_ns,
            RTDM_TIMERMODE_RELATIVE);
    if (ret) {
        rtdm_printk("PWM: error starting up-timer: %i\n", ret);
        return ret;
    }
    rtdm_printk("PWM: timers created\n");

    ///// Threads
    if (!rx) {
        task_mac_tx = kthread_run(mac_tx, "TX thread", "VLC_TX");
        if (IS_ERR(task_mac_tx)) {
            printk("Unable to start kernel threads. \n");
            ret = PTR_ERR(task_phy_decoding);
            task_mac_tx = NULL;
            task_phy_decoding = NULL;
            goto out;
        }
    }
    
    task_phy_decoding = kthread_run(phy_decoding, "RX thread", "VLC_DECODING");
    if (IS_ERR(task_phy_decoding)) {
        printk("Unable to start kernel threads. \n");
        ret = PTR_ERR(task_phy_decoding);
        task_mac_tx = NULL;
        task_phy_decoding = NULL;
        goto out;
    }

    printk("The VLC module has been initialized...\n\n");

    //get_device_ip(vlc_devs);
    build_routing_table = 1;

out:
    printk("------EXIT vlc_init_module------\n");
    if (ret)
        vlc_cleanup();

    return ret;
}

module_init(vlc_init_module);
module_exit(vlc_cleanup);
